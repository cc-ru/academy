use std::io::Read;
use std::path::Path;

use anyhow::Result;
use glam::Vec2;
use image::math::Rect;
use image::{GenericImage, GenericImageView, ImageFormat, RgbaImage};

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, AssetSource, Id, NewAssets};

/// Sprite asset
pub struct Sprite {
    /// Offset of the sprite accounting for cropping
    pub offset: Vec2,
    /// Image data, or None if the sprite is already uploaded
    pub image_data: Option<RgbaImage>,
}

impl Sprite {
    /// ID of the empty sprite used internally for rectangles with no texture
    pub const EMPTY: Id<Sprite> = Id::new(usize::max_value());
}

impl Asset for Sprite {}

impl AssetDefaultLoader for Sprite {
    type Loader = SpriteLoader;
}

/// Sprite loader
#[derive(Default)]
pub struct SpriteLoader;

/// Sprite extrusion
pub const EXTRUSION: u32 = 2;

impl AssetLoader<Sprite> for SpriteLoader {
    fn load(&self, _: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<Sprite> {
        let format = ImageFormat::from_path(path)?;
        let mut reader = source.load(path)?;
        let mut bytes = Vec::new();
        reader.read_to_end(&mut bytes)?;
        let image = image::load_from_memory_with_format(&bytes, format)?;
        let rgba = image.into_rgba();

        // cropping
        let (width, height) = rgba.dimensions();
        let check_col = |x| (0..height).all(|y| rgba.get_pixel(x, y).0[3] == 0);
        let x_start = (0..width).take_while(|&x| check_col(x)).count() as u32;
        let x_end = (0..width).rev().take_while(|&x| check_col(x)).count() as u32;
        let check_row = |y| (0..width).all(|x| rgba.get_pixel(x, y).0[3] == 0);
        let y_start = (0..height).take_while(|&y| check_row(y)).count() as u32;
        let y_end = (0..height).rev().take_while(|&y| check_row(y)).count() as u32;
        let crop_width = width - x_end - x_start;
        let crop_height = height - y_end - y_start;

        let e = EXTRUSION;
        let e2 = EXTRUSION * 2;

        let mut extruded = RgbaImage::new(crop_width + e2, crop_height + e2);
        let view = rgba.view(x_start, y_start, crop_width, crop_height);
        let _ = extruded.copy_from(&view, e, e);

        let rect = |x, y, width, height| Rect {
            x,
            y,
            width,
            height,
        };

        for v in 0..e {
            extruded.copy_within(rect(e, 0, 1, crop_height + e2), v, 0);
            extruded.copy_within(
                rect(e + crop_width - 1, 0, 1, crop_height + e2),
                crop_width + e2 - v - 1,
                0,
            );
            extruded.copy_within(rect(0, e, crop_width + e2, 1), 0, v);
            extruded.copy_within(
                rect(0, e + crop_height - 1, crop_width + e2, 1),
                0,
                crop_height + e2 - v - 1,
            );
        }

        Ok(Sprite {
            offset: Vec2::new(x_start as f32 + e as f32, y_start as f32 + e as f32),
            image_data: Some(extruded),
        })
    }
}
