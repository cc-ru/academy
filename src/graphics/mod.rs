//! Graphics subsystem

mod atlas;
mod drawlist;
mod encoder;
mod font;
mod resources;
mod sprite;

pub use self::drawlist::{BlendMode, DrawCommand, DrawList, DrawSprite, DrawText};
pub use self::encoder::Encoder;
pub use self::font::{Font, FontLoader};
pub use self::sprite::{Sprite, SpriteLoader};

use std::ops::Range;

use anyhow::{anyhow, Context, Result};
use glam::{Mat3, Vec2};
use glium::glutin::event_loop::EventLoop;
use glium::glutin::window::WindowBuilder;
use glium::glutin::ContextBuilder;
use glium::index::{NoIndices, PrimitiveType};
use glium::texture::ClientFormat;
use glium::uniforms::{MagnifySamplerFilter, MinifySamplerFilter};
use glium::{
    Blend, BlendingFunction, Display, DrawParameters, LinearBlendingFactor, Program, Rect, Surface,
    VertexBuffer,
};
use rusttype::{GlyphId, PositionedGlyph};

use self::atlas::TextureAtlas;
use self::resources::GlStorage;
use self::sprite::EXTRUSION;
use crate::assets::{Assets, Id};
use crate::util::{Aabr, AabrTree, FontUtil};

/// Time between frames
pub struct DeltaTime(pub f32);

/// Window logical resolution
pub struct WindowResolution(pub Vec2);

/// Handles all the graphics
pub struct Graphics {
    display: Display,
    storage: GlStorage,
    dpi_factor: f64,

    sprite_program: Program,
    sprite_instances: Vec<Instance>,
    sprite_batches: Vec<Range<u32>>,
    sprite_blend: BlendMode,

    text_program: Program,
    glyph_instances: Vec<Instance>,
    glyph_batches: Vec<Range<u32>>,
    glyph_keys: Vec<GlyphKey>,
    glyph_atlas: TextureAtlas<GlyphKey>,
    non_flushed_text: AabrTree,

    drawcalls: Vec<DrawCall>,
    square_vertices: VertexBuffer<Vertex>,
    scissor_rect: Rect,
}

impl Graphics {
    /// Open a window with an associated OpenGL context and initialize the graphics subsystem
    pub fn new(window_config: WindowBuilder, event_loop: &EventLoop<()>) -> Result<Graphics> {
        let context_builder = ContextBuilder::new().with_multisampling(4).with_vsync(true);
        let display = Display::new(window_config, context_builder, event_loop)
            .context("Cannot initialize display")?;

        let gl_window = display.gl_window();
        let dpi_factor = gl_window.window().scale_factor().trunc();
        drop(gl_window);

        Ok(Graphics {
            storage: GlStorage::new(&display)?,
            dpi_factor,

            sprite_program: Program::from_source(
                &display,
                include_str!("./shaders/generic.vert"),
                include_str!("./shaders/sprite.frag"),
                None,
            )
            .context("Cannot compile sprite shader program")?,
            sprite_instances: Vec::new(),
            sprite_batches: Vec::new(),
            sprite_blend: BlendMode::Alpha,

            text_program: Program::from_source(
                &display,
                include_str!("./shaders/generic.vert"),
                include_str!("./shaders/text.frag"),
                None,
            )
            .context("Cannot compile text shader program")?,
            glyph_instances: Vec::new(),
            glyph_batches: Vec::new(),
            glyph_keys: Vec::new(),
            glyph_atlas: TextureAtlas::new(&display, ClientFormat::U8)?,
            non_flushed_text: AabrTree::new(),

            drawcalls: Vec::new(),
            square_vertices: VertexBuffer::immutable(&display, &Vertex::SQUARE[..])?,
            scissor_rect: Rect::default(),

            display,
        })
    }

    /// Get window resolution
    pub fn resolution(&self) -> Vec2 {
        let gl_window = self.display.gl_window();
        let window = gl_window.window();
        let physical_resolution = window.inner_size();
        let logical_resolution = physical_resolution.to_logical(self.dpi_factor);
        Vec2::new(logical_resolution.width, logical_resolution.height)
    }

    /// Render the specified draw list, leaving it empty
    pub fn render(&mut self, list: &mut DrawList, assets: &mut Assets) -> Result<()> {
        let resolution = self.resolution();
        let dpi_factor = self.dpi_factor as f32;

        self.storage.maintain(&self.display, assets)?;

        let proj = build_proj_matrix(resolution);
        let transformed = evaluate_transforms(&list.commands, proj);

        let fullscreen_scissor = build_scissor(
            resolution.y(),
            dpi_factor,
            &Aabr::new(Vec2::zero(), resolution),
        );

        self.scissor_rect = fullscreen_scissor;

        for (cmd, view_proj, view) in transformed {
            match *cmd {
                DrawCommand::DrawSprite(cmd) => {
                    if self.sprite_blend != cmd.blend {
                        self.end_sprite_batch();
                        self.sprite_blend = cmd.blend;
                    }

                    let instance = Instance::from_sprite(cmd, &self.storage, &assets, &view_proj);

                    let size = Vec2::new(instance.in_uv_rect[2], instance.in_uv_rect[3]);
                    let aabr = Aabr::from_pos_extent(cmd.position, size).transform(&view);
                    if self.non_flushed_text.any_overlap(aabr) {
                        self.end_all_batches();
                    }

                    self.sprite_instances.push(instance);
                }

                DrawCommand::DrawText(ref cmd) => {
                    let font = assets.get_by_id(cmd.font).unwrap();
                    let aabr = match font.text_aabr(cmd.position, cmd.size, &cmd.text) {
                        Some(v) => v,
                        None => continue,
                    };

                    self.non_flushed_text.insert(aabr, ());

                    let pos = rusttype::Point {
                        x: cmd.position.x() * dpi_factor,
                        y: cmd.position.y() * dpi_factor,
                    };
                    let scale = rusttype::Scale::uniform(cmd.size * dpi_factor);

                    let glyphs = font.layout(&cmd.text, scale, pos);
                    for glyph in glyphs {
                        let bb = match glyph.pixel_bounding_box() {
                            Some(v) => v,
                            None => continue,
                        };

                        let key = GlyphKey::new(cmd.font, &glyph);
                        let instance = Instance::from_glyph(bb, cmd.color, dpi_factor, &view_proj);
                        self.glyph_instances.push(instance);
                        self.glyph_keys.push(key);

                        let dims = (bb.width() as u32, bb.height() as u32);
                        self.glyph_atlas.add(key, dims, |data| {
                            glyph.draw(|x, y, coverage| {
                                let value = (coverage * 255.0) as u8;
                                let idx = (y as usize) * (bb.width() as usize) + (x as usize);
                                data[idx] = value;
                            });
                        });
                    }
                }

                DrawCommand::SetScissor(scissor) => {
                    self.end_all_batches();
                    self.scissor_rect =
                        build_scissor(resolution.y(), dpi_factor, &scissor.transform(&view));
                }

                DrawCommand::ClearScissor => {
                    self.end_all_batches();
                    self.scissor_rect = fullscreen_scissor;
                }

                _ => {}
            }
        }

        self.end_all_batches();

        self.glyph_atlas.deallocate_dead();
        self.glyph_atlas.flush(&self.display)?;

        for (glyph, key) in self.glyph_instances.iter_mut().zip(&self.glyph_keys) {
            let rect = self
                .glyph_atlas
                .get_rect_f32(key)
                .expect("no glyph in atlas");
            glyph.in_uv_rect = rect;
        }

        let glyph_instance_buffer = VertexBuffer::immutable(&self.display, &self.glyph_instances)?;
        let sprite_instance_buffer =
            VertexBuffer::immutable(&self.display, &self.sprite_instances)?;

        let mut frame = self.display.draw();
        frame.clear_color(0.02, 0.02, 0.02, 1.0);

        for call in self.drawcalls.drain(..) {
            let alpha_blend = get_blend(BlendMode::Alpha);

            match call {
                DrawCall::Sprite(batch_id, blend, scissor) => {
                    let dims = self.storage.atlas.texture.dimensions();
                    let uniforms = glium::uniform! {
                        u_atlas: self.storage.atlas.texture.sampled()
                            .minify_filter(MinifySamplerFilter::Linear)
                            .magnify_filter(MagnifySamplerFilter::Linear),
                        u_tex_size: [dims.0 as f32, dims.1 as f32]
                    };
                    let range = &self.sprite_batches[batch_id];
                    let slice = sprite_instance_buffer
                        .slice(range.start as _..range.end as _)
                        .unwrap();
                    let ibuf = slice
                        .per_instance()
                        .map_err(|_| anyhow!("Instancing is not supported"))?;
                    let params = DrawParameters {
                        blend: get_blend(blend),
                        scissor: Some(scissor),
                        ..Default::default()
                    };
                    frame.draw(
                        (&self.square_vertices, ibuf),
                        NoIndices(PrimitiveType::TrianglesList),
                        &self.sprite_program,
                        &uniforms,
                        &params,
                    )?;
                }
                DrawCall::Text(batch_id, scissor) => {
                    let uniforms = glium::uniform! {
                        u_atlas: self.glyph_atlas.texture.sampled()
                            .minify_filter(MinifySamplerFilter::Linear)
                            .magnify_filter(MagnifySamplerFilter::Linear)
                    };
                    let range = &self.glyph_batches[batch_id];
                    let slice = glyph_instance_buffer
                        .slice(range.start as _..range.end as _)
                        .unwrap();
                    let ibuf = slice
                        .per_instance()
                        .map_err(|_| anyhow!("Instancing is not supported"))?;
                    let params = DrawParameters {
                        blend: alpha_blend,
                        scissor: Some(scissor),
                        ..Default::default()
                    };
                    frame.draw(
                        (&self.square_vertices, ibuf),
                        NoIndices(PrimitiveType::TrianglesList),
                        &self.text_program,
                        &uniforms,
                        &params,
                    )?;
                }
            };
        }

        frame.finish()?;

        list.reset();
        self.sprite_instances.clear();
        self.sprite_batches.clear();
        self.glyph_instances.clear();
        self.glyph_keys.clear();
        self.glyph_batches.clear();
        self.drawcalls.clear();

        Ok(())
    }

    fn end_all_batches(&mut self) {
        self.end_sprite_batch();
        self.end_text_batch();
    }

    fn end_sprite_batch(&mut self) {
        let start = self.sprite_batches.last().map(|last| last.end).unwrap_or(0);
        let end = self.sprite_instances.len() as u32;
        self.sprite_batches.push(start..end);
        self.drawcalls.push(DrawCall::Sprite(
            self.sprite_batches.len() - 1,
            self.sprite_blend,
            self.scissor_rect,
        ));
    }

    fn end_text_batch(&mut self) {
        let start = self.glyph_batches.last().map(|last| last.end).unwrap_or(0);
        let end = self.glyph_instances.len() as u32;
        self.glyph_batches.push(start..end);
        self.drawcalls.push(DrawCall::Text(
            self.glyph_batches.len() - 1,
            self.scissor_rect,
        ));
        self.non_flushed_text.clear();
    }
}

fn get_blend(mode: BlendMode) -> Blend {
    let constant_value = (0.0, 0.0, 0.0, 0.0);
    match mode {
        BlendMode::Alpha => Blend {
            color: BlendingFunction::Addition {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::OneMinusSourceAlpha,
            },
            alpha: BlendingFunction::Addition {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::OneMinusSourceAlpha,
            },
            constant_value,
        },
        BlendMode::Multiply => Blend {
            color: BlendingFunction::Addition {
                source: LinearBlendingFactor::DestinationColor,
                destination: LinearBlendingFactor::Zero,
            },
            alpha: BlendingFunction::Addition {
                source: LinearBlendingFactor::DestinationColor,
                destination: LinearBlendingFactor::Zero,
            },
            constant_value,
        },
        BlendMode::Subtract => Blend {
            color: BlendingFunction::ReverseSubtraction {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::One,
            },
            alpha: BlendingFunction::ReverseSubtraction {
                source: LinearBlendingFactor::Zero,
                destination: LinearBlendingFactor::One,
            },
            constant_value,
        },
        BlendMode::Add => Blend {
            color: BlendingFunction::Addition {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::One,
            },
            alpha: BlendingFunction::Addition {
                source: LinearBlendingFactor::Zero,
                destination: LinearBlendingFactor::One,
            },
            constant_value,
        },
        BlendMode::Lighten => Blend {
            color: BlendingFunction::Max,
            alpha: BlendingFunction::Max,
            constant_value,
        },
        BlendMode::Darken => Blend {
            color: BlendingFunction::Min,
            alpha: BlendingFunction::Min,
            constant_value,
        },
        BlendMode::Screen => Blend {
            color: BlendingFunction::Addition {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::OneMinusSourceColor,
            },
            alpha: BlendingFunction::Addition {
                source: LinearBlendingFactor::One,
                destination: LinearBlendingFactor::OneMinusSourceColor,
            },
            constant_value,
        },
        BlendMode::Replace => Blend {
            color: BlendingFunction::AlwaysReplace,
            alpha: BlendingFunction::AlwaysReplace,
            constant_value,
        },
    }
}

enum DrawCall {
    Sprite(usize, BlendMode, Rect),
    Text(usize, Rect),
}

fn build_proj_matrix(res: Vec2) -> Mat3 {
    Mat3::from_scale_angle_translation(
        Vec2::new(2.0 / res.x(), -2.0 / res.y()),
        0.0,
        Vec2::new(-1.0, 1.0),
    )
}

fn evaluate_transforms(
    commands: &[DrawCommand],
    proj: Mat3,
) -> impl Iterator<Item = (&'_ DrawCommand, Mat3, Mat3)> + Clone + '_ {
    let mut stack = Vec::new();
    let mut view = Mat3::identity();
    let mut view_proj = proj;
    commands.iter().flat_map(move |command| match command {
        DrawCommand::PushMatrix => {
            stack.push(view);
            None
        }
        DrawCommand::Transform(mat) => {
            view = *mat * view;
            view_proj = proj * view;
            None
        }
        DrawCommand::PopMatrix => {
            view = stack.pop().expect("nothing to pop");
            view_proj = proj * view;
            None
        }
        _ => Some((command, view_proj, view)),
    })
}

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct Vertex {
    in_pos: [f32; 2],
}

impl Vertex {
    const SQUARE: [Vertex; 6] = [
        Vertex { in_pos: [0.0, 0.0] },
        Vertex { in_pos: [1.0, 0.0] },
        Vertex { in_pos: [0.0, 1.0] },
        Vertex { in_pos: [1.0, 0.0] },
        Vertex { in_pos: [1.0, 1.0] },
        Vertex { in_pos: [0.0, 1.0] },
    ];
}

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct Instance {
    in_transform_col0_1: [f32; 4],
    in_transform_col2: [f32; 2],
    in_uv_rect: [f32; 4],
    in_color: [f32; 4],
}

impl Instance {
    fn from_mvp(mvp: Mat3, uv: [f32; 4], col: [f32; 4]) -> Instance {
        let [col0, col1, col2] = mvp.to_cols_array_2d();
        Instance {
            in_transform_col0_1: [col0[0], col0[1], col1[0], col1[1]],
            in_transform_col2: [col2[0], col2[1]],
            in_uv_rect: uv,
            in_color: col,
        }
    }

    fn from_sprite(
        mut sprite: DrawSprite,
        storage: &GlStorage,
        assets: &Assets,
        view_proj: &Mat3,
    ) -> Instance {
        let offset = assets
            .get_by_id(sprite.sprite)
            .map(|v| v.offset)
            .unwrap_or_default();
        sprite.position += offset;

        let uv_rect = storage.atlas.get_rect(&sprite.sprite).unwrap();
        let (w, h) = storage.atlas.texture.dimensions();

        let (size, uv_rect) = if sprite.sprite == Sprite::EMPTY {
            let uv_rect = [
                (uv_rect.min.x as f32 + 0.5) / w as f32,
                (uv_rect.min.y as f32 + 0.5) / h as f32,
                0.0,
                0.0,
            ];
            (Vec2::splat(1.0), uv_rect)
        } else {
            let e = EXTRUSION as f32;
            let e2 = EXTRUSION as f32 * 2.0;
            let width = uv_rect.width() as f32 - e2;
            let height = uv_rect.height() as f32 - e2;
            let uv_rect = [
                (uv_rect.min.x as f32 + e) / w as f32,
                (uv_rect.min.y as f32 + e) / h as f32,
                (uv_rect.width() as f32 - e2) / w as f32,
                (uv_rect.height() as f32 - e2) / h as f32,
            ];
            (Vec2::new(width, height), uv_rect)
        };

        let model = Mat3::from_scale_angle_translation(
            sprite.scale,
            sprite.rotation,
            sprite.rotation_origin + sprite.position,
        ) * Mat3::from_scale_angle_translation(size, 0.0, -sprite.rotation_origin);
        let mvp = *view_proj * model;

        Instance::from_mvp(mvp, uv_rect, sprite.color)
    }

    fn from_glyph(
        bb: rusttype::Rect<i32>,
        color: [f32; 4],
        dpi: f32,
        view_proj: &Mat3,
    ) -> Instance {
        let size = Vec2::new(bb.width() as f32 / dpi, bb.height() as f32 / dpi);
        let pos = Vec2::new(bb.min.x as f32 / dpi, bb.min.y as f32 / dpi);
        let model = Mat3::from_scale_angle_translation(size, 0.0, pos);
        let mvp = *view_proj * model;
        Instance::from_mvp(mvp, [0.0; 4], color)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct GlyphKey {
    font_id: Id<Font>,
    glyph_id: GlyphId,
    subpixel_offset: (u32, u32),
    size: u32,
}

impl GlyphKey {
    fn new(font_id: Id<Font>, glyph: &PositionedGlyph<'_>) -> GlyphKey {
        GlyphKey {
            font_id,
            glyph_id: glyph.id(),
            subpixel_offset: (
                glyph.position().x.fract().to_bits(),
                glyph.position().y.fract().to_bits(),
            ),
            size: glyph.scale().y.to_bits(),
        }
    }
}

fn build_scissor(height: f32, scale: f32, scissor: &Aabr) -> Rect {
    let size = scissor.extent();
    Rect {
        left: (scissor.min.x() * scale) as u32,
        bottom: ((height - scissor.min.y() - size.y()) * scale) as u32,
        width: (size.x() * scale) as u32,
        height: (size.y() * scale) as u32,
    }
}
