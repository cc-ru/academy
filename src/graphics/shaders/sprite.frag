#version 120

varying vec4 v_color;
varying vec2 v_uv;

uniform sampler2D u_atlas;
uniform vec2 u_tex_size;

vec4 cubic(float v) {
    vec4 n = vec4(1.0, 2.0, 3.0, 4.0) - v;
    vec4 s = n * n * n;
    float x = s.x;
    float y = s.y - 4.0 * s.x;
    float z = s.z - 4.0 * s.y + 6.0 * s.x;
    float w = 6.0 - x - y - z;
    return vec4(x, y, z, w) * (1.0/6.0);
}

void main() {
    vec2 uv = v_uv;
    vec2 inv_tex_size = 1.0 / u_tex_size;

    uv = uv * u_tex_size - 0.5;

    vec2 fxy = fract(uv);
    uv -= fxy;

    vec4 xcubic = cubic(fxy.x);
    vec4 ycubic = cubic(fxy.y);

    vec4 c = uv.xxyy + vec2 (-0.5, +1.5).xyxy;

    vec4 s = vec4(xcubic.xz + xcubic.yw, ycubic.xz + ycubic.yw);
    vec4 of = c + vec4 (xcubic.yw, ycubic.yw) / s;

    of *= inv_tex_size.xxyy;

    vec4 sample0 = texture2D(u_atlas, of.xz);
    vec4 sample1 = texture2D(u_atlas, of.yz);
    vec4 sample2 = texture2D(u_atlas, of.xw);
    vec4 sample3 = texture2D(u_atlas, of.yw);

    float sx = s.x / (s.x + s.y);
    float sy = s.z / (s.z + s.w);

    vec4 col = mix(mix(sample3, sample2, sx), mix(sample1, sample0, sx), sy);
    gl_FragColor = col * v_color;
}
