#version 120

varying vec4 v_color;
varying vec2 v_uv;

uniform sampler2D u_atlas;

void main() {
    float alpha = texture2D(u_atlas, v_uv).r;
    gl_FragColor = vec4(v_color.rgb * alpha, v_color.a * alpha);
}
