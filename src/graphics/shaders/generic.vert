#version 120

attribute vec2 in_pos;

attribute vec4 in_transform_col0_1;
attribute vec2 in_transform_col2;
attribute vec4 in_uv_rect;
attribute vec4 in_color;

varying vec4 v_color;
varying vec2 v_uv;

void main() {
    v_color = vec4(in_color.rgb * in_color.a, in_color.a);
    v_uv = in_pos * in_uv_rect.zw + in_uv_rect.xy;

    mat3 transform = mat3(mat3x2(
        in_transform_col0_1.xy,
        in_transform_col0_1.zw,
        in_transform_col2
    ));

    gl_Position = vec4(transform * vec3(in_pos, 1.0), 1.0);
}
