use glam::Vec2;
use serde::Deserialize;

use crate::assets::Id;
use crate::util::Aabr;

use super::{Font, Sprite};

/// List of all drawing commands generated every frame and given to the graphics engine
#[derive(Debug, Default)]
pub struct DrawList {
    /// List of commands
    pub commands: Vec<DrawCommand>,
}

impl DrawList {
    /// Reset the draw list
    pub fn reset(&mut self) {
        self.commands.clear();
    }
}

/// Draw command
#[derive(Debug)]
pub enum DrawCommand {
    /// Push a copy of the current transformation matrix on stack
    PushMatrix,
    /// Transform current matrix
    Transform(glam::Mat3),
    /// Pop transformation matrix from stack and set it as the current matrix. Panics when there's nothing to pop
    PopMatrix,
    /// Draw a sprite
    DrawSprite(DrawSprite),
    /// Draw some text
    DrawText(DrawText),
    /// Set scissor rect
    SetScissor(Aabr),
    /// Clear scissor
    ClearScissor,
}

/// Options for drawing a single sprite
#[derive(Clone, Copy, Debug)]
pub struct DrawSprite {
    /// Sprite ID
    pub sprite: Id<Sprite>,
    /// Top-left corner
    pub position: Vec2,
    /// Rotation origin relative to position
    pub rotation_origin: Vec2,
    /// Rotation in radians
    pub rotation: f32,
    /// Scale along each axis
    pub scale: Vec2,
    /// Color (tint)
    pub color: [f32; 4],
    /// Blending mode
    pub blend: BlendMode,
}

impl Default for DrawSprite {
    fn default() -> Self {
        DrawSprite {
            sprite: Sprite::EMPTY,
            position: Vec2::zero(),
            rotation_origin: Vec2::zero(),
            rotation: 0.0,
            scale: Vec2::one(),
            color: [1.0; 4],
            blend: BlendMode::Alpha,
        }
    }
}

/// Options for drawing a line of text
#[derive(Clone, Debug)]
pub struct DrawText {
    /// Font ID
    pub font: Id<Font>,
    /// Baseline
    pub position: Vec2,
    /// Font size
    pub size: f32,
    /// Color
    pub color: [f32; 4],
    /// Text to draw
    pub text: String,
}

/// Blending mode
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize)]
pub enum BlendMode {
    /// Alpha blending (normal)
    Alpha,
    /// Multiply
    Multiply,
    /// Subtract
    Subtract,
    /// Add
    Add,
    /// Lighten
    Lighten,
    /// Darken
    Darken,
    /// Screen
    Screen,
    /// Replace
    Replace,
}
