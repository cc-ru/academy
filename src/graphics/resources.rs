//! Graphics resources

use anyhow::Result;
use glium::texture::{ClientFormat, SrgbTexture2d};
use glium::Display;
use image::Rgba;

use super::atlas::TextureAtlas;
use super::Sprite;
use crate::assets::{Assets, Id};

#[derive(Clone, Copy, Debug, glium_derive::Vertex)]
#[repr(C)]
pub struct MeshVertex {
    #[glium(attr = "in_pos")]
    pos: [f32; 3],
    #[glium(attr = "in_normal")]
    norm: [f32; 3],
    #[glium(attr = "in_tex")]
    tex: [f32; 2],
}

/// Resource storage
#[derive(Debug)]
pub struct GlStorage {
    /// Sprite atlas
    pub atlas: TextureAtlas<Id<Sprite>, SrgbTexture2d>,
}

impl GlStorage {
    /// Create an empty `GlStorage`
    pub fn new(display: &Display) -> Result<GlStorage> {
        let mut atlas = TextureAtlas::new(display, ClientFormat::U8U8U8U8)?;
        atlas.add_raw(Sprite::EMPTY, (1, 1), &[255, 255, 255, 255]);
        Ok(GlStorage { atlas })
    }

    /// Upload data on GPU. Perform cleanup
    pub fn maintain(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        self.maintain_sprites(display, assets)?;
        Ok(())
    }

    fn maintain_sprites(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        self.cleanup_sprites(display, assets)?;
        self.upload_sprites(display, assets)
    }

    fn upload_sprites(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        for (handle, sprite) in assets.iter_mut::<Sprite>() {
            let mut rgba = match sprite.image_data.take() {
                Some(v) => v,
                None => continue,
            };

            // convert to premultiplied alpha
            for Rgba([r, g, b, a]) in rgba.pixels_mut() {
                let a = f32::from(*a) / 255.0;
                *r = (f32::from(*r) * a) as u8;
                *g = (f32::from(*g) * a) as u8;
                *b = (f32::from(*b) * a) as u8;
            }

            let (w, h) = rgba.dimensions();
            let samples = rgba.as_flat_samples();
            let bytes = samples.as_slice();
            self.atlas.add_raw(handle.id(), (w, h), &bytes);
        }

        self.atlas.flush(display)?;

        Ok(())
    }

    fn cleanup_sprites(&mut self, _: &Display, assets: &mut Assets) -> Result<()> {
        assets.cleanup_with(|id, _, _| {
            self.atlas.deallocate(&id);
        });

        Ok(())
    }
}
