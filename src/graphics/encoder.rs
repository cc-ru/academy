use std::borrow::Borrow;

use glam::{Mat3, Vec2};
use palette::{Pixel, Srgba};

use super::{BlendMode, DrawCommand, DrawList, DrawSprite, DrawText, Font, Sprite};
use crate::assets::Id;
use crate::util::Aabr;

/// Draw list encoder
#[derive(Debug)]
pub struct Encoder<'a> {
    list: &'a mut DrawList,
}

impl<'a> Encoder<'a> {
    /// Create an encoder for the specified draw list
    pub fn new(list: &'a mut DrawList) -> Encoder<'a> {
        Encoder { list }
    }

    /// Add a command to the underlying draw list
    pub fn add_command(&mut self, command: DrawCommand) {
        self.list.commands.push(command);
    }

    /// Push a copy of the current transformation matrix on the stack
    pub fn push_matrix(&mut self) {
        self.add_command(DrawCommand::PushMatrix);
    }

    /// Pop a transformation matrix from the stack and set is as the current matrix
    pub fn pop_matrix(&mut self) {
        self.add_command(DrawCommand::PopMatrix);
    }

    /// Post-transform current view by some matrix
    pub fn transform(&mut self, matrix: Mat3) {
        self.add_command(DrawCommand::Transform(matrix));
    }

    /// Translate current view by an offset
    pub fn translate(&mut self, offset: impl Into<Vec2>) {
        self.transform(Mat3::from_scale_angle_translation(
            Vec2::one(),
            0.0,
            offset.into(),
        ));
    }

    /// Scale current view by the values specified in vector on each axis
    pub fn scale(&mut self, axis: impl Into<Vec2>) {
        self.transform(Mat3::from_scale_angle_translation(
            axis.into(),
            0.0,
            Vec2::zero(),
        ));
    }

    /// Rotate current view by a unit complex
    pub fn rotate(&mut self, radians: f32) {
        self.transform(Mat3::from_scale_angle_translation(
            Vec2::one(),
            radians,
            Vec2::zero(),
        ));
    }

    /// Draw a sprite
    pub fn sprite(&mut self, id: impl Into<Id<Sprite>>, position: impl Into<Vec2>) {
        self.add_command(DrawCommand::DrawSprite(DrawSprite {
            sprite: id.into(),
            position: position.into(),
            rotation_origin: Vec2::zero(),
            rotation: 0.0,
            scale: Vec2::one(),
            color: [1.0; 4],
            blend: BlendMode::Alpha,
        }));
    }

    /// Draw a rectangle
    pub fn rect(
        &mut self,
        position: impl Into<Vec2>,
        size: impl Into<Vec2>,
        color: impl Into<Srgba>,
    ) {
        self.add_command(DrawCommand::DrawSprite(DrawSprite {
            sprite: Sprite::EMPTY,
            position: position.into(),
            rotation_origin: Vec2::zero(),
            rotation: 0.0,
            scale: size.into(),
            color: color.into().into_linear().into_format().into_raw(),
            blend: BlendMode::Alpha,
        }));
    }

    /// Draw text
    pub fn text(
        &mut self,
        font: impl Into<Id<Font>>,
        position: impl Into<Vec2>,
        size: f32,
        color: impl Into<Srgba>,
        text: impl Into<String>,
    ) {
        self.add_command(DrawCommand::DrawText(DrawText {
            font: font.into(),
            position: position.into(),
            size,
            color: color.into().into_linear().into_format().into_raw(),
            text: text.into(),
        }));
    }

    /// Draw 1px thick AABR outline
    pub fn aabr_outline(&mut self, aabr: impl Borrow<Aabr>, color: impl Into<Srgba>) {
        let color = color.into();
        let aabr = aabr.borrow();
        let w = Vec2::new(aabr.width(), 1.0);
        let h = Vec2::new(1.0, aabr.height());
        self.rect(aabr.min, w, color);
        self.rect(aabr.min + Vec2::new(0.0, aabr.height()), w, color);
        self.rect(aabr.min, h, color);
        self.rect(aabr.min + Vec2::new(aabr.width() - 1.0, 0.0), h, color);
    }
}
