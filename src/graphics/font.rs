use anyhow::{anyhow, Result};

use crate::assets::{Asset, AssetDefaultLoader, AssetReader, SimpleAssetLoader};

/// Font
pub type Font = rusttype::Font<'static>;

impl Asset for Font {}

impl AssetDefaultLoader for Font {
    type Loader = FontLoader;
}

/// Font loader
#[derive(Default)]
pub struct FontLoader;

impl SimpleAssetLoader<Font> for FontLoader {
    fn load(&self, mut reader: impl AssetReader) -> Result<Font> {
        let mut buf = Vec::new();
        reader.read_to_end(&mut buf)?;
        Font::try_from_vec(buf).ok_or_else(|| anyhow!("Failed to load font"))
    }
}
