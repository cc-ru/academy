//! Input system

use std::path::Path;

use anyhow::Result;
use fxhash::FxHashMap;
use glam::Vec2;
use glium::glutin::event::{
    ElementState, KeyboardInput, ModifiersState, MouseButton, MouseScrollDelta, VirtualKeyCode,
    WindowEvent,
};
use serde::{Deserialize, Serialize};

use crate::util::Aabr;

/// Analog axis binding, controls a value from -1 to 1
#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum AnalogBinding {
    /// Key pair binding
    Key {
        /// Controls positive direction (like W for walking)
        positive: VirtualKeyCode,
        /// Controls negative direction (like S for walking)
        negative: VirtualKeyCode,
    },
}

/// Key with modifiers
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct KeyCombination {
    /// Main key
    pub main: VirtualKeyCode,
    /// Modifiers (alt, shift, ctrl, logo)
    #[serde(default)]
    pub modifiers: ModifiersState,
}

/// Action binding. Can be in hold mode or in press mode depending on the situation
#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum ActionBinding {
    /// Key binding
    Key(KeyCombination),
    /// Mouse binding
    Mouse(MouseButton),
}

/// Controls map
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Controls {
    analog: FxHashMap<String, AnalogBinding>,
    action: FxHashMap<String, ActionBinding>,
}

impl Controls {
    /// Parse controls from file
    pub fn from_file(path: impl AsRef<Path>) -> Result<Controls> {
        let s = std::fs::read_to_string(path)?;
        Ok(ron::from_str(&s)?)
    }
}

struct AnalogBindingEntry {
    is_positive_down: bool,
    is_negative_down: bool,
}

struct ActionBindingEntry {
    has_pressed: bool,
    has_released: bool,
    is_enabled: bool,
}

type BindingEntry = (Option<(usize, bool)>, Option<usize>);

/// Input manager
pub struct Input {
    controls: Controls,
    action_entries: Vec<ActionBindingEntry>,
    analog_entries: Vec<AnalogBindingEntry>,
    action_name_to_id: FxHashMap<String, usize>,
    analog_name_to_id: FxHashMap<String, usize>,
    key_to_binding: FxHashMap<KeyCombination, BindingEntry>,
    mouse_to_action: FxHashMap<MouseButton, usize>,
    wheel_delta: f32,
    dpi_factor: f64,
    mouse_pos: Vec2,
    prev_mouse_pos: Vec2,
    modifiers: ModifiersState,
}

impl Input {
    /// Create a new input manager
    pub fn new(controls: Controls) -> Input {
        let mut this = Input {
            action_entries: controls
                .action
                .iter()
                .map(|_| ActionBindingEntry {
                    has_pressed: false,
                    has_released: false,
                    is_enabled: false,
                })
                .collect(),
            analog_entries: controls
                .analog
                .iter()
                .map(|_| AnalogBindingEntry {
                    is_positive_down: false,
                    is_negative_down: false,
                })
                .collect(),
            action_name_to_id: controls
                .action
                .iter()
                .enumerate()
                .map(|(i, (name, _))| (name.clone(), i))
                .collect(),
            analog_name_to_id: controls
                .analog
                .iter()
                .enumerate()
                .map(|(i, (name, _))| (name.clone(), i))
                .collect(),
            controls,
            key_to_binding: FxHashMap::default(),
            mouse_to_action: FxHashMap::default(),
            wheel_delta: 0.0,
            dpi_factor: 1.0,
            mouse_pos: Vec2::zero(),
            prev_mouse_pos: Vec2::zero(),
            modifiers: ModifiersState::empty(),
        };

        this.update_key_map();
        this.update_mouse_map();

        this
    }

    fn update_key_map(&mut self) {
        self.key_to_binding.clear();

        for (name, &binding) in self.controls.analog.iter() {
            let AnalogBinding::Key { positive, negative } = binding;
            let positive = KeyCombination {
                main: positive,
                modifiers: ModifiersState::empty(),
            };

            let negative = KeyCombination {
                main: negative,
                modifiers: ModifiersState::empty(),
            };

            let mut entry = self.key_to_binding.entry(positive).or_insert((None, None));
            let id = self.analog_name_to_id[name];
            entry.0 = Some((id, true));

            let mut entry = self.key_to_binding.entry(negative).or_insert((None, None));
            entry.0 = Some((id, false));
        }

        for (name, &binding) in self.controls.action.iter() {
            if let ActionBinding::Key(key) = binding {
                let mut entry = self.key_to_binding.entry(key).or_insert((None, None));
                let id = self.action_name_to_id[name];
                entry.1 = Some(id);
            }
        }
    }

    fn update_mouse_map(&mut self) {
        self.mouse_to_action.clear();
        for (name, &binding) in self.controls.action.iter() {
            if let ActionBinding::Mouse(button) = binding {
                let id = self.action_name_to_id[name];
                self.mouse_to_action.insert(button, id);
            }
        }
    }

    /// Executed at the beginning of each frame
    pub fn reset(&mut self) {
        self.prev_mouse_pos = self.mouse_pos;
        self.wheel_delta = 0.0;

        for entry in &mut self.action_entries {
            entry.has_pressed = false;
            entry.has_released = false;
        }
    }

    /// Change DPI factor
    pub fn set_dpi_factor(&mut self, dpi_factor: f64) {
        self.dpi_factor = dpi_factor
    }

    /// Handle a window event
    pub fn handle_event(&mut self, event: &WindowEvent<'_>) {
        match *event {
            WindowEvent::MouseInput { state, button, .. } => self.handle_mouse_event(state, button),
            WindowEvent::KeyboardInput { input, .. } => self.handle_key_event(input),
            WindowEvent::ModifiersChanged(mods) => self.modifiers = mods,
            WindowEvent::CursorMoved { position, .. } => {
                let pos = position.to_logical(self.dpi_factor);
                self.mouse_pos = Vec2::new(pos.x, pos.y);
            }
            WindowEvent::MouseWheel { delta, .. } => match delta {
                MouseScrollDelta::LineDelta(_, y) => self.wheel_delta += y,
                MouseScrollDelta::PixelDelta(v) => self.wheel_delta += v.y as f32,
            },
            _ => {}
        }
    }

    fn handle_mouse_event(&mut self, state: ElementState, button: MouseButton) {
        let id = match self.mouse_to_action.get(&button) {
            Some(&v) => v,
            None => return,
        };

        let entry = &mut self.action_entries[id];
        match state {
            ElementState::Pressed => {
                entry.has_pressed = true;
                entry.is_enabled = true;
            }
            ElementState::Released => {
                entry.has_released = true;
                entry.is_enabled = false;
            }
        }
    }

    fn handle_key_event(&mut self, input: KeyboardInput) {
        let keycode = match input.virtual_keycode {
            Some(v) => v,
            None => return,
        };

        let modifiers = self.modifiers;
        let combination = KeyCombination {
            main: keycode,
            modifiers,
        };

        let (analog, action) = match self.key_to_binding.get(&combination) {
            Some(&v) => v,
            None => return,
        };

        if let Some((id, positive)) = analog {
            let entry = &mut self.analog_entries[id];
            match input.state {
                ElementState::Pressed => {
                    if positive {
                        entry.is_positive_down = true;
                    } else {
                        entry.is_negative_down = true;
                    }
                }
                ElementState::Released => {
                    if positive {
                        entry.is_positive_down = false;
                    } else {
                        entry.is_negative_down = false;
                    }
                }
            }
        }

        if let Some(id) = action {
            let entry = &mut self.action_entries[id];
            match input.state {
                ElementState::Pressed => {
                    entry.has_pressed = true;
                    entry.is_enabled = true;
                }
                ElementState::Released => {
                    entry.has_released = true;
                    entry.is_enabled = false;
                }
            }
        }
    }

    /// Get analog axis value
    pub fn get_analog_value(&self, name: &str) -> f32 {
        match self.analog_name_to_id.get(name) {
            Some(&v) => {
                let pos = self.analog_entries[v].is_positive_down;
                let neg = self.analog_entries[v].is_negative_down;
                if pos == neg {
                    0.0
                } else if pos {
                    1.0
                } else {
                    -1.0
                }
            }
            _ => {
                log::warn!("no such analog axis: {}", name);
                0.0
            }
        }
    }

    /// Check whether the action binding was pressed
    pub fn has_action_pressed(&self, name: &str) -> bool {
        match self.action_name_to_id.get(name) {
            Some(&v) => self.action_entries[v].has_pressed,
            _ => {
                log::warn!("no such action: {}", name);
                false
            }
        }
    }

    /// Check whether the action binding was released
    pub fn has_action_released(&self, name: &str) -> bool {
        match self.action_name_to_id.get(name) {
            Some(&v) => self.action_entries[v].has_released,
            _ => {
                log::warn!("no such action: {}", name);
                false
            }
        }
    }

    /// Check whether the action is enabled (keys down)
    pub fn is_action_enabled(&self, name: &str) -> bool {
        match self.action_name_to_id.get(name) {
            Some(&v) => self.action_entries[v].is_enabled,
            _ => {
                log::warn!("no such action: {}", name);
                false
            }
        }
    }

    /// Get mouse position
    pub fn mouse_pos(&self) -> Vec2 {
        self.mouse_pos
    }

    /// Has mouse entered the given AABR?
    pub fn has_mouse_entered(&self, aabr: &Aabr) -> bool {
        !aabr.contains_point(self.prev_mouse_pos) && aabr.contains_point(self.mouse_pos)
    }

    /// Has mouse left the given AABR?
    pub fn has_mouse_left(&self, aabr: &Aabr) -> bool {
        aabr.contains_point(self.prev_mouse_pos) && !aabr.contains_point(self.mouse_pos)
    }

    /// Mouse wheel delta
    pub fn wheel_delta(&self) -> f32 {
        self.wheel_delta
    }
}
