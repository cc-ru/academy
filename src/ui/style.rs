use std::collections::VecDeque;
use std::convert::TryFrom;

use fxhash::FxHashMap;
use glam::Vec2;
use palette::{Hsva, Srgba};

use crate::util::Interpolate;

/// Style property
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Property {
    /// Color property
    Color(Hsva),
    /// Number property
    Number(f32),
    /// 2D vector property
    Vector(Vec2),
}

impl From<Hsva> for Property {
    fn from(color: Hsva) -> Property {
        Property::Color(color)
    }
}

impl From<Srgba> for Property {
    fn from(color: Srgba) -> Property {
        Property::Color(color.into())
    }
}

impl From<f32> for Property {
    fn from(value: f32) -> Property {
        Property::Number(value)
    }
}

impl From<Vec2> for Property {
    fn from(value: Vec2) -> Property {
        Property::Vector(value)
    }
}

impl TryFrom<Property> for Hsva {
    type Error = ();

    fn try_from(value: Property) -> Result<Hsva, ()> {
        match value {
            Property::Color(color) => Ok(color),
            _ => Err(()),
        }
    }
}

impl TryFrom<Property> for f32 {
    type Error = ();

    fn try_from(value: Property) -> Result<f32, ()> {
        match value {
            Property::Number(v) => Ok(v),
            _ => Err(()),
        }
    }
}

impl TryFrom<Property> for Vec2 {
    type Error = ();

    fn try_from(value: Property) -> Result<Vec2, ()> {
        match value {
            Property::Vector(v) => Ok(v),
            _ => Err(()),
        }
    }
}

/// Stores all properties
#[derive(Clone, Debug, Default)]
pub struct Style {
    properties: FxHashMap<(&'static str, &'static str), Property>,
}

impl Style {
    /// Create an empty style
    pub fn new() -> Style {
        Style::default()
    }

    /// Add a property
    pub fn add<P>(&mut self, scope: &'static str, name: &'static str, value: P)
    where
        P: Into<Property>,
    {
        self.properties.insert((scope, name), value.into());
    }

    /// Get value of the property
    pub fn get<V: TryFrom<Property>>(&self, scope: &'static str, name: &'static str) -> Option<V> {
        self.properties
            .get(&(scope, name))
            .and_then(|v| V::try_from(*v).ok())
    }
}

/// Stores the list of styles and retrives the most recent properties
#[derive(Debug, Default)]
pub struct StyleChain<'a> {
    /// Inner deque
    pub deque: VecDeque<&'a Style>,
}

impl<'a> StyleChain<'a> {
    /// Get value of the property
    pub fn get<V: TryFrom<Property>>(&self, scope: &'static str, name: &'static str) -> V {
        let order = self.deque.iter();
        order
            .flat_map(|s| s.get(scope, name))
            .next()
            .ok_or_else(|| format!("no such property: {}/{}", scope, name))
            .unwrap()
    }

    /// Interpolate the property between to scopes
    pub fn interpolate<V: TryFrom<Property> + Interpolate>(
        &self,
        from: &'static str,
        to: &'static str,
        property: &'static str,
        time: f32,
    ) -> V {
        let from = self.get(from, property);
        let to = self.get(to, property);
        V::interpolate(&from, &to, time)
    }
}

/// Construct a new style
///
/// ```
/// let style = style! {
///     button {
///         background: Hsva::new(0.0, 0.0, 0.8, 1.0),
///         foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
///     };
///
///     label {
///         background: Hsva::new(1.0, 1.0, 1.0, 0.0),
///         foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
///     };
/// };
/// ```
#[macro_export]
macro_rules! style {
    ($($scope:ident { $( $prop:ident : $value:expr, )+ };)*) => {{
        let mut style = $crate::ui::Style::new();
        $($(
            style.add(stringify!($scope), stringify!($prop), $value);
        )+)+
        style
    }};
}

/// Construct a new style, wrapped into `Lazy` to use in static variables
#[macro_export]
macro_rules! lazy_style {
    ($($scope:ident { $( $prop:ident : $value:expr, )+ };)*) => {
        once_cell::sync::Lazy::new(|| style!($($scope { $($prop: $value,)+ };)+))
    };
}
