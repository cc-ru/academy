use std::ops::{Deref, DerefMut};

use rusttype::Font;

use super::storage::WidgetStorage;
use super::{StyleChain, UiStorage, WidgetId};
use crate::assets::{Assets, Id};
use crate::graphics::Encoder;
use crate::input::Input;

/// UI context
pub struct UiContext<'a> {
    /// Encoder to write draw commands to
    pub encoder: &'a mut Encoder<'a>,
    /// Assets
    pub assets: &'a Assets,
    /// Default font (to be replaced with `Style`)
    pub font: Id<Font<'static>>,
    /// Id of the current widget
    pub id: WidgetId,
    /// Storage
    pub storage: &'a mut UiStorage,
    /// Input
    pub input: &'a Input,
    /// Time since last frame
    pub dt: f32,
    /// Style chain
    pub style: StyleChain<'a>,
}

impl UiContext<'_> {
    /// Get storage of the current widget
    pub fn storage(&mut self) -> WidgetStorage<'_> {
        self.storage.widget(&self.id)
    }
}

impl<'a> Deref for UiContext<'a> {
    type Target = Encoder<'a>;

    fn deref(&self) -> &Encoder<'a> {
        &self.encoder
    }
}

impl<'a> DerefMut for UiContext<'a> {
    fn deref_mut(&mut self) -> &mut Encoder<'a> {
        &mut self.encoder
    }
}
