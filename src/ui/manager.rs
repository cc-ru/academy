use std::collections::BTreeMap;

use glam::Vec2;

use super::widget::Geometry;
use super::{UiContext, Widget};
use crate::util::Aabr;

/// Widget constructor
pub trait WidgetCtor<'a> {
    fn create(self: Box<Self>, ctx: &mut UiContext<'a>) -> Box<dyn Widget<'a> + 'a>;
}

impl<'a, W, F> WidgetCtor<'a> for F
where
    F: FnOnce(&mut UiContext<'a>) -> W,
    W: Widget<'a>,
{
    fn create(self: Box<Self>, ctx: &mut UiContext<'a>) -> Box<dyn Widget<'a> + 'a> {
        Box::new((self)(ctx))
    }
}

struct UiRoot {
    ctor: Box<dyn for<'a> WidgetCtor<'a>>,
    aabr: Aabr,
}

/// UI Manager. Stores UI roots
#[derive(Default)]
pub struct UiManager {
    roots: BTreeMap<i32, Option<UiRoot>>,
}

impl UiManager {
    fn add_dyn_root(&mut self, ctor: Box<dyn for<'a> WidgetCtor<'a>>, order: i32, aabr: Aabr) {
        self.roots.insert(order, Some(UiRoot { ctor, aabr }));
    }

    /// Add an UI root
    pub fn add_root(&mut self, order: i32, pos: impl Into<Vec2>) -> RootBuilder<'_> {
        RootBuilder {
            manager: self,
            order,
            pos: pos.into(),
            size: None,
        }
    }

    /// Handle all the roots
    pub fn handle(&mut self, uctx: &mut UiContext<'_>) {
        for root in self.roots.values_mut() {
            let root = root.take().unwrap();
            let mut widget = root.ctor.create(uctx);
            let size = widget.size(uctx, root.aabr.extent());
            let aabr = Aabr::new(root.aabr.min, root.aabr.min + size);
            let geometry = Geometry {
                bounds: aabr,
                clamped_bounds: aabr,
                parent_clamped_bounds: Aabr::new(
                    Vec2::new(0.0, 0.0),
                    Vec2::new(f32::INFINITY, f32::INFINITY),
                ),
            };
            widget.update(uctx, &geometry);
            widget.draw(uctx, &geometry);
        }

        self.roots.clear();
    }
}

/// UI root builder
pub struct RootBuilder<'a> {
    manager: &'a mut UiManager,
    order: i32,
    pos: Vec2,
    size: Option<Vec2>,
}

impl<'a> RootBuilder<'a> {
    /// Set widget size
    pub fn with_size(mut self, size: impl Into<Vec2>) -> RootBuilder<'a> {
        self.size = Some(size.into());
        self
    }

    /// Finish building the root using the specified factory function
    pub fn widget<F>(self, factory: F)
    where
        F: for<'b> WidgetCtor<'b> + 'static,
    {
        let size = self.size.unwrap_or_else(|| Vec2::zero());
        let aabr = Aabr::new(self.pos, self.pos + size);
        self.manager
            .add_dyn_root(Box::new(factory), self.order, aabr);
    }
}
