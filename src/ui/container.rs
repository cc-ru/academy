use super::widget::*;
use super::{Style, UiContext};
use crate::util::{Orientation, SideOffsets};

/// Container of widgets
pub trait Container<'a> {
    /// The underlying `UiContext`.
    fn context(&mut self) -> &mut UiContext<'a>;

    /// Add a widget to the container
    fn widget<W: Widget<'a>>(&mut self, widget: W);

    /// Orientation of the container
    fn orientation(&self) -> Orientation;

    /// Add a style scope, referencing the given style
    fn style_scope<F>(&mut self, style: &'a Style, factory: F)
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let orientation = self.orientation();
        let widget = StyleScope::new(self.context(), style, orientation, factory);
        self.widget(widget);
    }

    /// Add a filler (aka spring)
    fn filler(&mut self, stretch: f32) {
        let filler = Filler::new(stretch, self.orientation());
        self.widget(filler)
    }

    /// Add a stack sub-container with given settings
    fn stack<F>(&mut self, settings: StackSettings<'a>, factory: F)
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let stack = Stack::new(self.context(), settings, factory);
        self.widget(stack);
    }

    /// Add a vertical stack
    fn vstack<F>(&mut self, factory: F)
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        self.stack(StackSettings::default(), factory)
    }

    /// Add a horizontal stack
    fn hstack<F>(&mut self, factory: F)
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        self.stack(
            StackSettings {
                orientation: Orientation::Horizontal,
                ..StackSettings::default()
            },
            factory,
        )
    }

    /// Add a padding box
    fn padding<F>(&mut self, padding: SideOffsets, factory: F)
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let widget = PaddingBox::new(self.context(), padding, factory);
        self.widget(widget);
    }

    /// Add a label
    fn label(&mut self, label: impl Into<String>) {
        let label = Label::new(self.context(), label.into());
        self.widget(label)
    }

    /// Add multiline text
    fn text(&mut self, text: impl Into<String>) {
        let text = Text::new(self.context(), text);
        self.widget(text)
    }

    /// Add a button
    fn button(&mut self, label: impl AsRef<str>, callback: impl FnMut() + 'a) {
        let button = Button::new(self.context(), label.as_ref(), callback);
        self.widget(button)
    }
}
