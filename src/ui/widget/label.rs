use glam::Vec2;
use once_cell::sync::Lazy;
use palette::Hsva;
use rusttype::Scale;

use super::{Geometry, Widget};
use crate::ui::{Style, UiContext};
use crate::util::FontUtil;

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    label {
        foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
        font_size: 20.0,
        line_height: 1.2,
    };
};

/// Label (text on one line)
pub struct Label {
    text: String,
    width: f32,
    height: f32,
    offset: f32,
}

impl Label {
    /// Create a new label
    pub fn new<'a>(uctx: &mut UiContext<'a>, text: String) -> Label {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("label", "font_size");
        let line_height = uctx.style.get::<f32>("label", "line_height");
        uctx.style.deque.pop_back();

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let v_metrics = font.v_metrics(Scale::uniform(font_size));
        let width = font.text_width(font_size, &text);
        let height = font_size * line_height;

        Label {
            text,
            width,
            height,
            offset: height / 2.0 - v_metrics.descent,
        }
    }
}

impl<'a> Widget<'a> for Label {
    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("label", "font_size");
        let foreground = uctx.style.get::<Hsva>("label", "foreground");
        uctx.style.deque.pop_back();

        let pos = geometry.bounds.min + Vec2::new(0.0, self.offset);
        let font = uctx.font;
        uctx.text(font, pos, font_size, foreground, &self.text);
    }

    fn size(&mut self, _: &UiContext<'a>, _: Vec2) -> Vec2 {
        Vec2::new(self.width, self.height)
    }
}
