use glam::Vec2;

use super::{Geometry, UiContext, Widget};
use crate::ui::Container;
use crate::util::{Aabr, Orientation};

/// How should stack align its children along direction orthogonal to orientation
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Alignment {
    /// Top or left
    Start,
    /// Center
    Center,
    /// Bottom or right
    End,
}

#[derive(Clone, Copy, Debug, PartialEq)]
/// Settings for the `Stack` container
pub struct StackSettings<'a> {
    /// Orientation (vertical by default)
    pub orientation: Orientation,
    /// Alignment (start by default)
    pub alignment: Alignment,
    /// Stretch factor of the stack itself (1.0 by default)
    pub stretch_factor: f32,
    /// ID of the stack
    pub id: Option<&'a str>,
}

impl Default for StackSettings<'static> {
    fn default() -> StackSettings<'static> {
        StackSettings {
            orientation: Orientation::Vertical,
            alignment: Alignment::Start,
            stretch_factor: 1.0,
            id: None,
        }
    }
}

struct ChildEntry<'a> {
    widget: Box<dyn Widget<'a>>,
    size: Vec2,
    offset: Vec2,
    size_final: bool,
}

/// Stack widget. Lays out its children similar to a flexbox container in CSS
pub struct Stack<'a> {
    children: Vec<ChildEntry<'a>>,
    size: Vec2,
    settings: StackSettings<'a>,
}

/// Stack container used to fill stack with child widgets
pub struct StackContainer<'a, 'r> {
    stack: &'r mut Stack<'a>,
    uctx: &'r mut UiContext<'a>,
}

impl<'a> Container<'a> for StackContainer<'a, '_> {
    fn context(&mut self) -> &mut UiContext<'a> {
        self.uctx
    }

    fn widget<W: Widget<'a>>(&mut self, widget: W) {
        let widget = Box::new(widget);
        let child = ChildEntry {
            widget,
            size: Vec2::zero(),
            offset: Vec2::zero(),
            size_final: false,
        };

        self.stack.children.push(child);
    }

    fn orientation(&self) -> Orientation {
        self.stack.settings.orientation
    }
}

impl<'a> Stack<'a> {
    /// Create a new stack with the given settings and a factory function to fill it
    pub fn new<F>(uctx: &mut UiContext<'a>, settings: StackSettings<'a>, factory: F) -> Stack<'a>
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let id = settings.id;
        let mut stack = Stack {
            children: Vec::new(),
            size: Vec2::new(0.0, 0.0),
            settings,
        };

        if let Some(id) = id {
            uctx.id.push(id);
        }

        let mut filler = StackContainer {
            stack: &mut stack,
            uctx,
        };

        factory(&mut filler);

        if id.is_some() {
            uctx.id.pop();
        }

        stack
    }

    fn layout_static(&mut self, uctx: &UiContext<'a>) {
        self.size = Vec2::zero();

        for child in &mut self.children {
            if child.widget.stretch_factor() > f32::EPSILON {
                child.size_final = false;
                child.size = Vec2::zero();
                continue;
            }

            child.size = child.widget.size(uctx, Vec2::zero());
            child.size_final = true;

            if self.settings.orientation == Orientation::Vertical {
                self.size.set_x(self.size.x().max(child.size.x()));
                self.size.set_y(self.size.y() + child.size.y());
            } else {
                self.size.set_y(self.size.y().max(child.size.y()));
                self.size.set_x(self.size.x() + child.size.x());
            }
        }
    }

    fn total_stretch(&self) -> f32 {
        self.children
            .iter()
            .map(|c| c.widget.stretch_factor())
            .sum()
    }

    #[inline(always)]
    fn layout_vertical(&mut self, uctx: &UiContext<'a>, hint: Vec2) {
        assert_eq!(self.settings.orientation, Orientation::Vertical);

        let mut avail_height = (hint.y() - self.size.y()).max(0.0);
        let mut total_stretch = self.total_stretch();

        let mut num_i = 0;
        while total_stretch > f32::EPSILON && num_i < 5 {
            num_i += 1;

            let mut stretch_unit = (avail_height / total_stretch).max(0.0);

            for child in &mut self.children {
                if child.size_final {
                    continue;
                }

                let stretch = child.widget.stretch_factor();

                let old_height = child.size.y();
                let height = old_height + stretch_unit * stretch;
                child.size = child.widget.size(uctx, Vec2::new(hint.x(), height));
                let size_diff = child.size.y() - old_height;
                self.size.set_x(self.size.x().max(child.size.x()));
                self.size.set_y(self.size.y() + size_diff);
                avail_height -= size_diff;

                if (child.size.y() - height).abs() > f32::EPSILON || size_diff < f32::EPSILON {
                    child.size_final = true;
                    total_stretch -= stretch;
                    if total_stretch <= f32::EPSILON {
                        break;
                    }
                    stretch_unit = (avail_height / total_stretch).max(0.0);
                }
            }

            if avail_height < f32::EPSILON {
                break;
            }
        }

        let mut y = 0.0;
        for child in &mut self.children {
            child.offset.set_y(y);
            child.offset.set_x(match self.settings.alignment {
                Alignment::Start => 0.0,
                Alignment::Center => (self.size.x() - child.size.x()) / 2.0,
                Alignment::End => self.size.x() - child.size.x(),
            });
            y += child.size.y();
        }
    }

    #[inline(always)]
    fn layout_horizontal(&mut self, uctx: &UiContext<'a>, hint: Vec2) {
        assert_eq!(self.settings.orientation, Orientation::Horizontal);

        let mut avail_width = (hint.x() - self.size.x()).max(0.0);
        let mut total_stretch = self.total_stretch();

        let mut num_i = 0;
        while total_stretch > f32::EPSILON && num_i < 5 {
            num_i += 1;

            let mut stretch_unit = (avail_width / total_stretch).max(0.0);

            for child in &mut self.children {
                if child.size_final {
                    continue;
                }

                let stretch = child.widget.stretch_factor();

                let old_width = child.size.x();
                let width = old_width + stretch_unit * stretch;
                child.size = child.widget.size(uctx, Vec2::new(width, hint.y()));
                let size_diff = child.size.x() - old_width;
                self.size.set_y(self.size.y().max(child.size.y()));
                self.size.set_x(self.size.x() + size_diff);
                avail_width -= size_diff;

                if (child.size.x() - width).abs() > f32::EPSILON || size_diff < f32::EPSILON {
                    child.size_final = true;
                    total_stretch -= stretch;
                    if total_stretch <= f32::EPSILON {
                        break;
                    }

                    stretch_unit = (avail_width / total_stretch).max(0.0);
                }
            }

            if avail_width < f32::EPSILON {
                break;
            }
        }

        let mut x = 0.0;
        for child in &mut self.children {
            child.offset.set_x(x);
            child.offset.set_y(match self.settings.alignment {
                Alignment::Start => 0.0,
                Alignment::Center => (self.size.y() - child.size.y()) / 2.0,
                Alignment::End => self.size.y() - child.size.y(),
            });
            x += child.size.x();
        }
    }

    fn layout(&mut self, uctx: &UiContext<'a>, hint: Vec2) {
        self.layout_static(uctx);
        match self.settings.orientation {
            Orientation::Vertical => self.layout_vertical(uctx, hint),
            Orientation::Horizontal => self.layout_horizontal(uctx, hint),
        }
    }

    fn foreach_child(
        &mut self,
        uctx: &mut UiContext<'a>,
        geometry: &Geometry,
        mut f: impl FnMut(&mut UiContext<'a>, &mut dyn Widget<'a>, &Geometry),
    ) {
        for child in &mut self.children {
            let min = geometry.bounds.min + child.offset;
            let max = min + child.size;
            let bounds = Aabr::new(min, max);
            let clamped_bounds = bounds.intersection(&geometry.clamped_bounds);
            let child_geometry = Geometry {
                bounds,
                clamped_bounds,
                parent_clamped_bounds: geometry.clamped_bounds,
            };

            if let Some(id) = child.widget.id() {
                uctx.id.push(id);
            }

            f(uctx, &mut *child.widget, &child_geometry);

            if child.widget.id().is_some() {
                uctx.id.pop();
            }
        }
    }
}

impl<'a> Widget<'a> for Stack<'a> {
    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        self.foreach_child(uctx, geometry, |uctx, widget, geometry| {
            widget.update(uctx, geometry)
        });
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        self.foreach_child(uctx, geometry, |uctx, widget, geometry| {
            if geometry.bounds.area() > 0.0 {
                widget.draw(uctx, geometry)
            }
        });
    }

    fn id(&self) -> Option<&str> {
        self.settings.id
    }

    fn size(&mut self, uctx: &UiContext<'a>, hint: Vec2) -> Vec2 {
        self.layout(uctx, hint);
        self.size
    }

    fn stretch_factor(&self) -> f32 {
        self.settings.stretch_factor
    }
}
