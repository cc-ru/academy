use glam::Vec2;

use super::{Geometry, Stack, StackContainer, StackSettings, Widget};
use crate::ui::style::Style;
use crate::ui::UiContext;
use crate::util::Orientation;

/// Style scope
pub struct StyleScope<'a> {
    /// The underlying style
    pub style: &'a Style,
    inner: Stack<'a>,
}

impl<'a> StyleScope<'a> {
    /// Create a new style scope
    pub fn new<F>(
        uctx: &mut UiContext<'a>,
        style: &'a Style,
        orientation: Orientation,
        factory: F,
    ) -> StyleScope<'a>
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let settings = StackSettings {
            orientation,
            ..Default::default()
        };
        uctx.style.deque.push_back(style);
        let inner = Stack::new(uctx, settings, factory);
        uctx.style.deque.pop_back();
        StyleScope { style, inner }
    }
}

impl<'a> Widget<'a> for StyleScope<'a> {
    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(self.style);
        self.inner.update(uctx, geometry);
        uctx.style.deque.pop_back();
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(self.style);
        self.inner.draw(uctx, geometry);
        uctx.style.deque.pop_back();
    }

    fn size(&mut self, uctx: &UiContext<'a>, hint: Vec2) -> Vec2 {
        self.inner.size(uctx, hint)
    }

    fn stretch_factor(&self) -> f32 {
        1.0
    }
}
