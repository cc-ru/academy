//! UI widgets

mod button;
mod filler;
mod label;
mod padding;
mod stack;
mod style_scope;
mod text;

pub use self::button::Button;
pub use self::filler::Filler;
pub use self::label::Label;
pub use self::padding::PaddingBox;
pub use self::stack::*;
pub use self::style_scope::StyleScope;
pub use self::text::Text;

use glam::Vec2;

use crate::ui::UiContext;
use crate::util::Aabr;

/// Widget
pub trait Widget<'a>: 'a {
    /// Unique identifier of the widget inside current scope, or `None`, if it doesn't define a new
    /// scope (the widget's storage will point to one of its ancestors).
    ///
    /// Default value is `None`.
    fn id(&self) -> Option<&str> {
        None
    }

    /// Update the widget. Called before `draw`. Technically, you could do everything inside
    /// `draw`, but it's better to separate big functions
    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let _ = (uctx, geometry);
    }

    /// Draw the widget. By default, doesn't do anything.
    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let _ = (uctx, geometry);
    }

    /// Get size of the widget, performing the layout if necessary. `hint` has a meaning only if
    /// the widget has a nonzero stretch factor
    fn size(&mut self, uctx: &UiContext<'a>, hint: Vec2) -> Vec2 {
        let _ = (uctx, hint);
        Vec2::new(0.0, 0.0)
    }

    /// Stretch factor of the widget.
    ///
    /// Has a meaning only inside a `Stack` container. Defines the
    /// amount of stretching along the stack orientation. By default is `0`, meaning that the
    /// widget width (for horizontal orientation), or height (for vertical) will be set to the
    /// minimum value.
    fn stretch_factor(&self) -> f32 {
        0.0
    }
}

/// Geometry of the widget (size, position, etc)
#[derive(Clone, Copy, Debug)]
pub struct Geometry {
    /// Widget bounds for drawing. The extents will always be between the minimum and the maximum
    /// sizes.
    pub bounds: Aabr,
    /// Widget bounds used for click detection. A subrectangle of parent's `clamped_bounds`.
    ///
    /// Will be not equal to `bounds` if the widget is inside a scroll view (or any other
    /// widget which uses scissor testing), or if the widget is too big to fit into its container.
    pub clamped_bounds: Aabr,
    /// Clamped bounds of the parent widget
    pub parent_clamped_bounds: Aabr,
}
