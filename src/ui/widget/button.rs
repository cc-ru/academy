use glam::Vec2;
use once_cell::sync::Lazy;
use palette::Hsva;
use rusttype::Scale;

use super::{Geometry, Widget};
use crate::ui::{Style, UiContext};
use crate::util::FontUtil;

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    button {
        foreground: Hsva::new(0.0, 0.0, 1.0, 1.0),
        font_size: 20.0,
        line_height: 1.3,
    };
};

/// Button
pub struct Button<'a> {
    callback: Box<dyn FnMut() + 'a>,
    label: String,
    id: String,
    width: f32,
    height: f32,
    offset: f32,
}

impl Button<'_> {
    /// Create a new button
    pub fn new<'a, F>(uctx: &mut UiContext<'a>, label: &str, callback: F) -> Button<'a>
    where
        F: FnMut() + 'a,
    {
        let mut split = label.rsplitn(2, "##");
        let id = split.next().unwrap_or(label).to_owned();
        let label = split.next().unwrap_or(label).to_owned();

        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("button", "font_size");
        let line_height = uctx.style.get::<f32>("button", "line_height");
        uctx.style.deque.pop_back();

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let v_metrics = font.v_metrics(Scale::uniform(font_size));
        let width = font.text_width(font_size, &label);
        let height = font_size * line_height;

        Button {
            callback: Box::new(callback),
            label,
            id,
            width: width + font_size,
            height,
            offset: height / 2.0 - v_metrics.descent,
        }
    }
}

impl<'a> Widget<'a> for Button<'a> {
    fn id(&self) -> Option<&str> {
        Some(&self.id)
    }

    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let rect_bounds = geometry.clamped_bounds;

        if rect_bounds.contains_point(uctx.input.mouse_pos())
            && uctx.input.has_action_pressed("ui.click")
        {
            (self.callback)();
        }
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("button", "font_size");
        let foreground = uctx.style.get::<Hsva>("button", "foreground");
        uctx.style.deque.pop_back();

        let pos = geometry.bounds.min + Vec2::new(0.5 * font_size, self.offset);
        let font = uctx.font;
        uctx.text(font, pos, font_size, foreground, &self.label);

        uctx.aabr_outline(&geometry.clamped_bounds, foreground);
    }

    fn size(&mut self, _: &UiContext<'a>, _: Vec2) -> Vec2 {
        Vec2::new(self.width, self.height)
    }
}
