//! Particle systems

#![allow(missing_docs)]

use std::marker::PhantomData;
use std::path::PathBuf;
use std::sync::Arc;

use glam::Vec2;
use legion::systems::CommandBuffer;
use legion::{component, system, Entity};
use rand::{Rng, SeedableRng};
use rand_distr::{Distribution, Normal, Uniform};
use rand_xoshiro::Xoshiro128Plus;
use serde::de::{Deserializer, SeqAccess, Visitor};
use serde::Deserialize;

use crate::assets::{Asset, AssetDefaultLoader, Assets, Handle, JsonLoader};
use crate::components::{
    CollisionRect, Position, Rotation, Scale, SpriteBlendMode, SpriteColor, SpriteOrigin,
    SpriteRotationOrigin, Velocity,
};
use crate::graphics::{BlendMode, DeltaTime, Sprite};
use crate::util::Aabr;

/// Defines an interpolation graph with at most 8 samples
#[derive(Clone, Copy, Debug)]
pub struct Graph<T> {
    /// The number of samples to uniformly distribute over the time period
    pub len: u8,
    /// Equal to 1 / len
    pub step: f32,
    /// Samples. Only first `len` values will be used, and the others will be ignored
    pub samples: [T; 8],
}

impl<T> Graph<T> {
    fn idx(&self, time: f32) -> usize {
        (time / self.step).max(0.0).min(self.len as f32 - 1.0) as usize
    }
}

fn lerp(a: f32, b: f32, t: f32) -> f32 {
    a + (b - a) * t
}

impl Graph<f32> {
    /// Sample a value at the specified time
    pub fn sample(&self, time: f32) -> f32 {
        let idx = self.idx(time);
        let len = self.len as usize;
        if idx == len - 1 {
            self.samples[len - 1]
        } else {
            let a = self.samples[idx];
            let b = self.samples[idx + 1];
            lerp(a, b, (time / self.step).fract())
        }
    }
}

impl Graph<[f32; 4]> {
    /// Sample a value at the specified time
    pub fn sample(&self, time: f32) -> [f32; 4] {
        let idx = self.idx(time);
        let len = self.len as usize;
        if idx == len - 1 {
            self.samples[len - 1]
        } else {
            let [r0, g0, b0, a0] = self.samples[idx];
            let [r1, g1, b1, a1] = self.samples[idx + 1];
            let t = (time / self.step).fract();
            [
                lerp(r0, r1, t),
                lerp(g0, g1, t),
                lerp(b0, b1, t),
                lerp(a0, a1, t),
            ]
        }
    }
}

struct GraphVisitor<T>(PhantomData<T>);

impl<'de, T: Default + Deserialize<'de>> Visitor<'de> for GraphVisitor<T> {
    type Value = Graph<T>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        formatter.write_str("an array")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut len = 0;
        let mut samples = <[T; 8]>::default();

        while let Some(v) = seq.next_element()? {
            samples[len as usize] = v;
            len += 1;
            if len == 8 {
                break;
            }
        }

        Ok(Graph {
            len,
            step: 1.0 / len as f32,
            samples,
        })
    }
}

impl<'de, T: Default + Deserialize<'de>> Deserialize<'de> for Graph<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(GraphVisitor(PhantomData))
    }
}

/// Contains minimum and maximum values
#[derive(Clone, Copy, Debug, Deserialize)]
pub struct MinMax<T> {
    /// Min
    pub min: T,
    /// Max
    pub max: T,
}

/// Emitter distribution type
#[derive(Clone, Copy, Debug, Deserialize)]
pub enum EmissionDistribution {
    /// Uniform distribution inside a rectangle
    Rectangle,
    /// Normal distribution
    Normal,
    /// Uniform distribution inside an ellipse
    Ellipse,
    /// Uniform distribution on the edge of an ellipse
    EllipseEdge,
    /// Uniform distribution on the edges of a rectangle
    RectangleEdge,
}

/// Emitter shape
#[derive(Clone, Copy, Debug, Deserialize)]
pub struct EmissionArea {
    /// Distribution
    pub distribution: EmissionDistribution,
    /// Maximum spawn distance from the center for uniform distribution, or standard deviation
    /// along each axis for normal distribution
    pub distance: [f32; 2],
    /// Rotation of the emission area in radians
    pub rotation: f32,
}

impl EmissionArea {
    /// Sample a point
    pub fn sample(&self, rng: &mut Xoshiro128Plus) -> Vec2 {
        let dist = self.distance;
        let [x, y] = match self.distribution {
            EmissionDistribution::Rectangle => [
                rng.gen_range(-dist[0], dist[0]),
                rng.gen_range(-dist[1], dist[1]),
            ],
            EmissionDistribution::Normal => [
                Normal::new(0.0, dist[0])
                    .map(|v| v.sample(rng))
                    .unwrap_or(0.0),
                Normal::new(0.0, dist[1])
                    .map(|v| v.sample(rng))
                    .unwrap_or(0.0),
            ],
            EmissionDistribution::Ellipse => {
                let r = rng.gen::<f32>().sqrt();
                let a = rng.gen::<f32>() * 2.0 * std::f32::consts::PI;
                let (sin, cos) = a.sin_cos();
                [r * cos * dist[0], r * sin * dist[1]]
            }
            EmissionDistribution::EllipseEdge => {
                let a = rng.gen::<f32>() * 2.0 * std::f32::consts::PI;
                let (sin, cos) = a.sin_cos();
                [cos * dist[0], sin * dist[1]]
            }
            EmissionDistribution::RectangleEdge => {
                let side = rng.gen_range(0u8, 4u8);
                match side {
                    0 => [rng.gen_range(-dist[0], dist[0]), -dist[1]],
                    1 => [rng.gen_range(-dist[0], dist[0]), dist[1]],
                    2 => [-dist[0], rng.gen_range(-dist[1], dist[1])],
                    _ => [dist[0], -rng.gen_range(-dist[1], dist[1])],
                }
            }
        };

        let (sin, cos) = self.rotation.sin_cos();
        Vec2::new(cos * x - sin * y, sin * x + cos * y)
    }
}

/// Particle system parameters
#[derive(Clone, Debug, Deserialize)]
pub struct ParticleSystemParams {
    /// Emission area
    pub emission_area: EmissionArea,
    /// Particle emission rate (in s¯¹)
    pub emission_rate: f32,
    /// Lifetime of the emitter (in s)
    pub emitter_lifetime: f32,
    // TODO:
    // /// Maximum amount of particles
    // pub max_particles: u32,
    /// Defines the size of particles over time, 1.0 being the normal size (a random value between
    /// particle_size.min and particle_size.max)
    pub particle_size_graph: SizeGraph,
    /// Defines the color of particles over time
    pub particle_color_graph: ColorGraph,
    /// Particle lifetime
    pub particle_lifetime: MinMax<Lifetime>,
    /// Particle size
    pub particle_size: MinMax<Size>,
    /// Particle spin (in rad/s)
    pub particle_spin: MinMax<Spin>,
    /// Particle tangential acceleration (perpendicular to direction)
    pub particle_tangential_acceleration: MinMax<TangentialAcceleration>,
    /// Particle radial acceleration (away from the emitter)
    pub particle_radial_acceleration: MinMax<RadialAcceleration>,
    /// Particle linear acceleration (along the direction)
    pub particle_linear_acceleration: MinMax<LinearAcceleration>,
    /// Particle initial speed
    pub particle_initial_speed: MinMax<f32>,
    /// Particle initial rotation (in rad)
    pub particle_initial_rotation: MinMax<f32>,
    /// Initial speed direction (normalized)
    pub initial_direction: [f32; 2],
    /// Spread of the spawned particle speed in radians along the initial_direction
    pub initial_spread: f32,
    /// TODO
    pub rel_wrt_center: bool,
    /// Sprite path
    pub sprite: SpriteDef, // TODO: allow multiple sprites and animations
}

/// Particle sprite
#[derive(Clone, Debug, Deserialize)]
pub struct SpriteDef {
    /// Path
    pub path: PathBuf,
    /// Sorting origin
    pub origin: [f32; 2],
    /// Rotation origin
    pub rotation_origin: [f32; 2],
    /// Collision rectangle
    pub collision_rect: [f32; 4],
    /// Blend mode
    pub blend_mode: BlendMode,
}

impl Asset for ParticleSystemParams {}
impl AssetDefaultLoader for ParticleSystemParams {
    type Loader = JsonLoader;
}

/// Particle size graph
#[derive(Clone, Debug, Deserialize)]
#[serde(transparent)]
pub struct SizeGraph(pub Arc<Graph<f32>>);

/// Particle color graph
#[derive(Clone, Debug, Deserialize)]
#[serde(transparent)]
pub struct ColorGraph(pub Arc<Graph<[f32; 4]>>);

/// Particle lifetime
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct Lifetime(pub f32);

/// Particle size
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct Size(pub f32);

/// Spin
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct Spin(pub f32);

/// Tangential acceleration (perpendicular to direction)
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct TangentialAcceleration(pub f32);

/// Radial acceleration (away from the emitter)
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct RadialAcceleration(pub f32);

/// Linear acceleration (along the direction)
#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(transparent)]
pub struct LinearAcceleration(pub f32);

/// Emitter position (used for radial acceleration calculation)
#[derive(Clone, Copy, Debug)]
pub struct EmitterPos(pub Vec2);

/// Total particle lifetime
#[derive(Clone, Copy, Debug)]
pub struct TotalLifetime(pub f32);

/// Particle marker
pub struct Particle;

/// Particles emitter
#[derive(Debug)]
pub struct ParticleEmitter {
    /// Parameters
    pub params: ParticleSystemParams,
    sprite: Handle<Sprite>,
    num_particles: u32,
    emit_additionally: u32,
    emitted_fract: f32,
    rng: Xoshiro128Plus,
}

impl ParticleEmitter {
    /// Create a new particle emitter
    pub fn new(assets: &Assets, params: ParticleSystemParams) -> ParticleEmitter {
        let sprite = assets.load(&params.sprite.path);
        ParticleEmitter {
            params,
            sprite,
            num_particles: 0,
            emit_additionally: 0,
            emitted_fract: 0.0,
            rng: Xoshiro128Plus::from_rng(rand::thread_rng()).unwrap(),
        }
    }

    /// Emit a burst of particles
    pub fn emit(&mut self, count: u32) {
        self.emit_additionally += count;
    }
}

fn emit_particle(cbuf: &mut CommandBuffer, emitter: &mut ParticleEmitter, epos: Vec2) {
    let rng = &mut emitter.rng;
    let params = &emitter.params;
    let pos = Position(epos + params.emission_area.sample(rng));

    let lifetime = Lifetime(rng.sample(Uniform::new_inclusive(
        params.particle_lifetime.min.0,
        params.particle_lifetime.max.0,
    )));

    let init_speed = rng.sample(Uniform::new_inclusive(
        params.particle_initial_speed.min,
        params.particle_initial_speed.max,
    ));

    let rot = Rotation(rng.sample(Uniform::new_inclusive(
        params.particle_initial_rotation.min,
        params.particle_initial_rotation.max,
    )));

    let mut angle = rng.sample(Uniform::new_inclusive(
        -params.initial_spread / 2.0,
        params.initial_spread / 2.0,
    ));

    if params.rel_wrt_center {
        let d = (pos.0 - epos).normalize();
        angle += d.y().atan2(d.x());
    }

    let [x, y] = params.initial_direction;
    let (sin, cos) = angle.sin_cos();
    let dir = Vec2::new(cos * x - sin * y, sin * x + cos * y);

    cbuf.push((
        Particle,
        pos,
        EmitterPos(epos),
        lifetime,
        TotalLifetime(lifetime.0),
        Velocity(dir * init_speed),
        Size(rng.sample(Uniform::new_inclusive(
            params.particle_size.min.0,
            params.particle_size.max.0,
        ))),
        Spin(rng.sample(Uniform::new_inclusive(
            params.particle_spin.min.0,
            params.particle_spin.max.0,
        ))),
        TangentialAcceleration(rng.sample(Uniform::new_inclusive(
            params.particle_tangential_acceleration.min.0,
            params.particle_tangential_acceleration.max.0,
        ))),
        RadialAcceleration(rng.sample(Uniform::new_inclusive(
            params.particle_radial_acceleration.min.0,
            params.particle_radial_acceleration.max.0,
        ))),
        LinearAcceleration(rng.sample(Uniform::new_inclusive(
            params.particle_linear_acceleration.min.0,
            params.particle_linear_acceleration.max.0,
        ))),
        rot,
        emitter.params.particle_size_graph.clone(),
        emitter.params.particle_color_graph.clone(),
        emitter.sprite.clone(),
        SpriteOrigin(emitter.params.sprite.origin.into()),
        SpriteRotationOrigin(emitter.params.sprite.rotation_origin.into()),
        SpriteBlendMode(emitter.params.sprite.blend_mode),
        Scale(1.0),
        SpriteColor([1.0; 4]),
        CollisionRect(Aabr::new(
            Vec2::new(
                params.sprite.collision_rect[0],
                params.sprite.collision_rect[1],
            ),
            Vec2::new(
                params.sprite.collision_rect[2],
                params.sprite.collision_rect[3],
            ),
        )),
    ));
}

#[system(for_each)]
pub fn emit_particles(
    cbuf: &mut CommandBuffer,
    emitter: &mut ParticleEmitter,
    &Position(pos): &Position,
    #[resource] &DeltaTime(time): &DeltaTime,
) {
    let mut to_emit = emitter.emit_additionally;
    emitter.emit_additionally = 0;

    if emitter.params.emitter_lifetime > 0.0 {
        to_emit += emitter.emitted_fract as u32;
        emitter.emitted_fract = emitter.emitted_fract.fract();

        let burst = emitter.params.emission_rate * time;
        to_emit += burst as u32;
        emitter.emitted_fract += burst.fract();
        emitter.params.emitter_lifetime -= time;
    }

    for _ in 0..to_emit {
        emit_particle(cbuf, emitter, pos);
    }

    emitter.num_particles += to_emit;
}

#[system(par_for_each)]
#[filter(component::<Particle>())]
pub fn accelerate_particles(
    Velocity(vel): &mut Velocity,
    &TangentialAcceleration(tan_acc): &TangentialAcceleration,
    &RadialAcceleration(rad_acc): &RadialAcceleration,
    &LinearAcceleration(lin_acc): &LinearAcceleration,
    &EmitterPos(epos): &EmitterPos,
    &Position(pos): &Position,
    #[resource] &DeltaTime(dt): &DeltaTime,
) {
    let vel_len2 = vel.length_squared();
    let norm_vel = if vel_len2 > f32::EPSILON {
        *vel / vel_len2.sqrt()
    } else {
        Vec2::zero()
    };

    let vel_perp = Vec2::new(norm_vel.y(), -norm_vel.x());
    let acc = tan_acc * vel_perp + norm_vel * lin_acc + (pos - epos).normalize() * rad_acc;
    *vel += acc * dt;
}

#[system(par_for_each)]
#[filter(component::<Particle>() & !component::<CollisionRect>())]
pub fn move_particles(
    &Velocity(vel): &Velocity,
    Position(pos): &mut Position,
    #[resource] &DeltaTime(dt): &DeltaTime,
) {
    *pos += vel * dt;
}

#[system(par_for_each)]
#[filter(component::<Particle>())]
pub fn spin_particles(
    &Spin(spin): &Spin,
    Rotation(rot): &mut Rotation,
    #[resource] &DeltaTime(dt): &DeltaTime,
) {
    *rot += spin * dt;
}

#[system(par_for_each)]
#[filter(component::<Particle>())]
pub fn update_particle_size(
    &Lifetime(cur): &Lifetime,
    &TotalLifetime(total): &TotalLifetime,
    &Size(size): &Size,
    SizeGraph(graph): &SizeGraph,
    Scale(scale): &mut Scale,
) {
    *scale = size * graph.sample(1.0 - cur / total);
}

#[system(par_for_each)]
#[filter(component::<Particle>())]
pub fn update_particle_color(
    &Lifetime(cur): &Lifetime,
    &TotalLifetime(total): &TotalLifetime,
    ColorGraph(graph): &ColorGraph,
    SpriteColor(col): &mut SpriteColor,
) {
    *col = graph.sample(1.0 - cur / total);
}

#[system(for_each)]
#[filter(component::<Particle>())]
pub fn kill_old_particles(
    cbuf: &mut CommandBuffer,
    &entity: &Entity,
    Lifetime(t): &mut Lifetime,
    #[resource] &DeltaTime(dt): &DeltaTime,
) {
    *t -= dt;
    if *t < 0.0 {
        cbuf.remove(entity);
    }
}
