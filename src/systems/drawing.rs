//! Drawing system

#![allow(missing_docs)]

use std::cmp::Ordering;

use glam::Vec2;
use legion::world::SubWorld;
use legion::{component, system, IntoQuery};
use rayon::slice::ParallelSliceMut;

use crate::assets::Handle;
use crate::camera::Camera;
use crate::components::{
    Position, Rotation, Scale, SpriteBlendMode, SpriteColor, SpriteOrigin, SpriteRotationOrigin,
};
use crate::graphics::{
    BlendMode, DrawCommand, DrawList, DrawSprite, Encoder, Sprite, WindowResolution,
};
use crate::input::Input;
use crate::map::{FloorTile, Map};

#[system]
pub fn draw_background(
    #[resource] drawlist: &mut DrawList,
    #[resource] camera: &Camera,
    #[resource] resolution: &WindowResolution,
    #[resource] map: &Map,
) {
    let mut encoder = Encoder::new(drawlist);

    encoder.push_matrix();
    camera.apply(&mut encoder, resolution.0);

    for chunk_pos in &map.chunk_order {
        let chunk = &map.chunks[chunk_pos];
        for y in 0..8 {
            for x in 0..8 {
                let idx = y * 8 + x;
                let sprite = &chunk.tiles.sprites[idx];
                encoder.sprite(
                    sprite,
                    [
                        chunk_pos.x as f32 * 1024.0 + x as f32 * 128.0,
                        chunk_pos.y as f32 * 1024.0 + y as f32 * 128.0,
                    ],
                );
            }
        }
    }

    encoder.pop_matrix();
}

#[system]
#[read_component(Position)]
#[read_component(Rotation)]
#[read_component(Scale)]
#[read_component(Handle<Sprite>)]
#[read_component(SpriteColor)]
#[read_component(SpriteOrigin)]
#[read_component(SpriteRotationOrigin)]
#[read_component(SpriteBlendMode)]
pub fn draw_foreground(
    world: &mut SubWorld<'_>,
    #[resource] drawlist: &mut DrawList,
    #[resource] camera: &Camera,
    #[resource] resolution: &WindowResolution,
    #[resource] map: &Map,
    #[state] order: &mut Vec<(Vec2, DrawSprite)>,
) {
    let mut encoder = Encoder::new(drawlist);
    let mut query = <(
        &Position,
        &Handle<Sprite>,
        Option<&SpriteColor>,
        Option<&SpriteOrigin>,
        Option<&SpriteRotationOrigin>,
        Option<&SpriteBlendMode>,
        Option<&Rotation>,
        Option<&Scale>,
    )>::query()
    .filter(!component::<FloorTile>());

    order.clear();

    query.for_each(world, |(pos, sprite, col, origin, rot_o, b, rot, scale)| {
        let position = origin.map(|origin| pos.0 - origin.0).unwrap_or(pos.0) * 32.0;
        let draw = DrawSprite {
            sprite: sprite.into(),
            position,
            rotation_origin: rot_o.map(|v| v.0 * 32.0).unwrap_or_else(Vec2::zero),
            rotation: rot.map(|v| v.0).unwrap_or(0.0),
            scale: Vec2::splat(scale.map(|v| v.0).unwrap_or(1.0)),
            color: col.map(|v| v.0).unwrap_or([1.0; 4]),
            blend: b.map(|v| v.0).unwrap_or(BlendMode::Alpha),
        };
        order.push((pos.0, draw));
    });

    let comparator = |a: Vec2, b: Vec2| {
        a.y()
            .partial_cmp(&b.y())
            .unwrap_or(Ordering::Greater)
            .then_with(|| a.x().partial_cmp(&b.x()).unwrap_or(Ordering::Greater))
    };

    order.par_sort_unstable_by(|(lhs, _), (rhs, _)| comparator(*lhs, *rhs));

    encoder.push_matrix();
    camera.apply(&mut encoder, resolution.0);

    let chunk_order = map.chunk_order.iter();
    let iterators = chunk_order
        .map(|pos| {
            let objects = map.chunks[pos].objects.iter();
            objects.map(|obj| {
                let draw = DrawSprite {
                    sprite: obj.sprite.id(),
                    position: (obj.pos - obj.origin) * 32.0,
                    ..Default::default()
                };

                (obj.pos, draw)
            })
        })
        .map(either::Left)
        .chain(std::iter::once(either::Right(order.drain(..))));

    // this code runs in O(NK log K) time where N is the number of objects in every iterator, and K
    // is the number of iterators. There are about 16 chunks loaded at a time, so I hope this will
    // run fast enough
    let merged = itertools::kmerge_by(
        iterators,
        |a: &(Vec2, DrawSprite), b: &(Vec2, DrawSprite)| comparator(a.0, b.0) == Ordering::Less,
    );

    for (_, draw) in merged {
        encoder.add_command(DrawCommand::DrawSprite(draw));
    }

    encoder.pop_matrix();
}

#[system]
pub fn debug_map_bvh(
    #[resource] drawlist: &mut DrawList,
    #[resource] camera: &Camera,
    #[resource] resolution: &WindowResolution,
    #[resource] map: &Map,
    #[resource] input: &Input,
    #[state] visible: &mut bool,
) {
    if input.has_action_pressed("debug.map-bvh") {
        *visible = !*visible;
    }

    if !*visible {
        return;
    }

    let mut encoder = Encoder::new(drawlist);

    encoder.push_matrix();
    camera.apply(&mut encoder, resolution.0);

    for chunk in map.chunks.values() {
        for branch in chunk.bvh.iter_branches() {
            encoder.aabr_outline(*branch * 32.0, (1.0, 0.0, 0.0, 0.1));
        }
        for (leaf, _) in chunk.bvh.iter_leaves() {
            encoder.aabr_outline(*leaf * 32.0, (1.0, 1.0, 0.0, 0.3));
        }
    }

    encoder.pop_matrix();
}
