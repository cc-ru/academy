//! Character

#![allow(missing_docs)]

use std::path::PathBuf;
use std::time::{Duration, Instant};

use fxhash::FxHashMap;
use glam::{Vec2, Vec3};
use legion::{component, system, IntoQuery, Resources, World};
use rand::seq::SliceRandom;

use crate::assets::{Assets, Handle};
use crate::audio::{Audio, Sound};
use crate::camera::Camera;
use crate::components::{Position, Velocity};
use crate::graphics::{DeltaTime, WindowResolution};
use crate::input::Input;
use crate::map::{ChunkPosition, Map, PrefabList};

/// Character component
pub struct Character {
    /// Movement speed
    pub speed: f32,
    /// Time when the character stepped last time. None if not moving
    pub last_step: Option<Instant>,
    /// Step sounds
    pub step_sounds: Vec<Handle<Sound>>,
}

/// Marker for the player controlled character
#[derive(Clone, Copy, Debug)]
pub struct PlayerCharacter;

const RUN_PACE: Duration = Duration::from_millis(300);

#[system(for_each)]
#[filter(component::<PlayerCharacter>())]
pub fn player_controls(character: &mut Character, vel: &mut Velocity, #[resource] input: &Input) {
    let x = input.get_analog_value("movement.x");
    let y = input.get_analog_value("movement.y");
    let v = Vec2::new(x, y);
    if v.abs_diff_eq(Vec2::zero(), 1e-6) {
        vel.0 = Vec2::zero();
        character.last_step = None;
    } else {
        vel.0 = v.normalize() * character.speed;
        if character.last_step.is_none() {
            character.last_step = Some(Instant::now() - RUN_PACE);
        }
    }
}

#[system(for_each)]
pub fn step_sounds(
    character: &mut Character,
    &Position(pos): &Position,
    #[resource] audio: &Audio,
) {
    let last_step = match &mut character.last_step {
        Some(v) => v,
        None => return,
    };

    if last_step.elapsed() >= RUN_PACE {
        let mut rng = rand::thread_rng();
        let sound = match character.step_sounds.choose(&mut rng) {
            Some(v) => v,
            None => return,
        };

        audio.play_sound_at(sound, pos.extend(0.0), 0.15);

        *last_step += RUN_PACE;
    }
}

#[system(for_each)]
#[filter(component::<PlayerCharacter>())]
pub fn center_on_player(
    pos: &Position,
    #[resource] camera: &mut Camera,
    #[resource] audio: &mut Audio,
) {
    camera.center = pos.0;
    let pos = pos.0.extend(1.7);
    let dist = Vec3::new(0.0, 0.1, 0.0);
    audio.set_ear_pos(pos - dist, pos + dist);
}

#[system]
pub fn camera_scale_control(
    #[resource] camera: &mut Camera,
    #[resource] input: &Input,
    #[resource] &DeltaTime(dt): &DeltaTime,
    #[state] target: &mut f32,
) {
    if input.wheel_delta().abs() > 0.0 {
        let new_target = (*target + input.wheel_delta() * 0.1).min(1.0).max(0.5);
        *target = new_target;
    }

    camera.scale += (*target - camera.scale) * 10.0 * dt;
}

pub fn manage_chunks_system(world: &mut World, resources: &mut Resources) {
    let &pos = <&Position>::query().iter(world).next().unwrap();

    let prefabs = resources.get::<FxHashMap<PathBuf, PrefabList>>().unwrap();
    let assets = resources.get::<Assets>().unwrap();
    let resolution = resources.get::<WindowResolution>().unwrap();
    let mut map = resources.get_mut::<Map>().unwrap();

    let center_chunk = ChunkPosition::new(
        (pos.0.x() / 32.0).floor() as i16,
        (pos.0.y() / 32.0).floor() as i16,
    );

    let x_chunks = (resolution.0.x() / 1024.0).ceil() as i16;
    let y_chunks = (resolution.0.y() / 1024.0).ceil() as i16;

    let chunks = map.chunk_order.iter().copied();
    let to_unload = chunks
        .filter(|&pos| {
            pos.x < center_chunk.x - x_chunks
                || pos.x > center_chunk.x + x_chunks
                || pos.y < center_chunk.y - y_chunks
                || pos.y > center_chunk.y + y_chunks
        })
        .collect::<Vec<_>>();

    for chunk in to_unload {
        map.unload_chunk(chunk, world);
    }

    for x in center_chunk.x - x_chunks..=center_chunk.x + x_chunks {
        for y in center_chunk.y - y_chunks..=center_chunk.y + y_chunks {
            let chunk = ChunkPosition::new(x, y);
            if !map.is_chunk_loaded(chunk) && map.has_chunk(chunk) {
                map.load_chunk(chunk, &prefabs, world, &assets).unwrap();
            }
        }
    }
}
