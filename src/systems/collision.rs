//! Collision detection and resolution

#![allow(missing_docs)]

use glam::Vec2;

use legion::system;

use crate::components::{CollisionRect, Position, Velocity};
use crate::graphics::DeltaTime;
use crate::map::Map;

#[system(par_for_each)]
pub fn collision(
    Position(pos): &mut Position,
    &Velocity(vel): &Velocity,
    &CollisionRect(aabr): &CollisionRect,
    #[resource] map: &Map,
    #[resource] &DeltaTime(dt): &DeltaTime,
) {
    *pos.x_mut() += vel.x() * dt;

    let mut aabr = aabr.translate(*pos);
    for chunk in map.chunks.values() {
        for (overlap, _) in chunk.bvh.iter_overlaps(aabr) {
            let md = aabr.minkowski_difference(overlap);
            let pv = md.closest_point_on_bounds(Vec2::zero());
            *pos.x_mut() -= pv.x();
            *aabr.min.x_mut() -= pv.x();
            *aabr.max.x_mut() -= pv.x();
        }
    }

    let sy = vel.y() * dt;
    *pos.y_mut() += sy;
    *aabr.min.y_mut() += sy;
    *aabr.max.y_mut() += sy;

    for chunk in map.chunks.values() {
        for (overlap, _) in chunk.bvh.iter_overlaps(aabr) {
            let md = aabr.minkowski_difference(overlap);
            let pv = md.closest_point_on_bounds(Vec2::zero());
            *pos.y_mut() -= pv.y();
            *aabr.min.y_mut() -= pv.y();
            *aabr.max.y_mut() -= pv.y();
        }
    }
}
