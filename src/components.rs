//! Various entity components

use std::io::{self, Read};
use std::path::PathBuf;
use std::sync::Arc;

use glam::Vec2;

use crate::graphics::BlendMode;
use crate::map::{Decodable, DecodableComponent};
use crate::util::Aabr;

/// World coordinates. A single unit is 32x32 pixels
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position(pub Vec2);

impl Decodable for Position {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(Position(Vec2::decode(source)?))
    }
}

impl DecodableComponent for Position {
    const ID: u16 = 0;
}

/// Velocity (in world space)
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Velocity(pub Vec2);

/// Rotation in radians
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Rotation(pub f32);

/// Scale
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Scale(pub f32);

/// Sprite color
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SpriteColor(pub [f32; 4]);

/// Path to the sprite. Will be replaced with Id<Sprite> after loading
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct SpritePath(pub Arc<PathBuf>);

impl Decodable for SpritePath {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(SpritePath(Arc::new(PathBuf::decode(source)?)))
    }
}

impl DecodableComponent for SpritePath {
    const ID: u16 = 1;
}

/// Sprite origin. Point on the texture which will be used for defining sprite position
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SpriteOrigin(pub Vec2);

impl Decodable for SpriteOrigin {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(SpriteOrigin(Vec2::decode(source)?))
    }
}

impl DecodableComponent for SpriteOrigin {
    const ID: u16 = 3;
}

/// Sprite rotation origin. Point on the texture which will be used for defining sprite rotation
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SpriteRotationOrigin(pub Vec2);

/// Collision rectangle
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct CollisionRect(pub Aabr);

impl Decodable for CollisionRect {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(CollisionRect(Aabr::decode(source)?))
    }
}

impl DecodableComponent for CollisionRect {
    const ID: u16 = 2;
}

/// Sprite blending mode
#[derive(Clone, Copy, Debug, Hash, PartialEq)]
pub struct SpriteBlendMode(pub BlendMode);
