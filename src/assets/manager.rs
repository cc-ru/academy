use std::any::{type_name, Any, TypeId};
use std::mem::{self, ManuallyDrop};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Condvar, Mutex};
use std::time::Duration;
use std::{ops, ptr, thread};

use anyhow::Result;
use crossbeam::channel::{self, Receiver, Sender};
use dashmap::DashMap;
use fxhash::{FxBuildHasher, FxHashMap};

use super::{
    Asset, AssetDefaultLoader, AssetLoader, AssetSource, Handle, HandleAllocator, Id,
    ProgressCounter, WeakHandle,
};
use crate::util::anymap::{AnyMap, DowncastUnchecked};

/// Asset manager
pub struct Assets {
    storages: AnyMap<dyn DynStorage>,
    source: Arc<dyn AssetSource>,
    sched_rx: Receiver<SchedAsset>,
    loaded_rx: Receiver<LoadedAsset>,
    new_assets: Arc<NewAssets>,
}

impl Assets {
    /// Create a new asset manager
    pub fn new<S: AssetSource>(source: S) -> Assets {
        let (loaded_tx, loaded_rx) = channel::unbounded();
        let (sched_tx, sched_rx) = channel::unbounded();
        Assets {
            storages: AnyMap::new(),
            source: Arc::new(source),
            sched_rx,
            loaded_rx,
            new_assets: Arc::new(NewAssets {
                handle_allocator: Arc::new(HandleAllocator::new()),
                handles: Arc::new(HandleMap::new()),
                sched_tx,
                loaded_tx,
                waiter: Waiter::new(),
                counter: Arc::new(Mutex::new(ProgressCounter::new())),
            }),
        }
    }

    fn storage<A: Asset>(&self) -> &AssetStorage<A> {
        self.storages.get::<AssetStorage<A>>().unwrap()
    }

    fn storage_mut<A: Asset>(&mut self) -> &mut AssetStorage<A> {
        self.storages.get_mut::<AssetStorage<A>>().unwrap()
    }

    /// Register asset type
    pub fn register<A: Asset>(&mut self) {
        self.storages.insert(AssetStorage::<A>::new())
    }

    /// Get an immutable reference to asset by its ID, if the asset exists. Panics if the asset
    /// type is not registered
    pub fn get_by_id<A: Asset>(&self, id: Id<A>) -> Option<&A> {
        self.storage().get_by_id(id)
    }

    /// Get a mutable reference to asset by its ID, if the asset exists. Panics if the asset
    /// type is not registered
    pub fn get_by_id_mut<A: Asset>(&mut self, id: Id<A>) -> Option<&mut A> {
        self.storage_mut().get_by_id_mut(id)
    }

    /// Get an immutable reference to an asset by handle to it
    pub fn get<A: Asset>(&self, handle: &Handle<A>) -> Option<&A> {
        self.storage().get(handle)
    }

    /// Get a mutable reference to an asset by handle to it
    pub fn get_mut<A: Asset>(&mut self, handle: &Handle<A>) -> Option<&mut A> {
        self.storage_mut().get_mut(handle)
    }

    /// Insert an asset returning handle to it
    pub fn insert<A: Asset>(&mut self, asset: A) -> Handle<A> {
        let handle = self.new_assets.handle_allocator.alloc_handle();
        self.storage_mut().insert(asset, &handle);
        handle
    }

    /// Remove assets with zero live handles
    pub fn cleanup(&mut self) {
        for storage in self.storages.values_mut() {
            storage.cleanup();
        }

        self.new_assets.handles.cleanup();
    }

    /// Schedule an asset for loading
    pub fn load<A: AssetDefaultLoader>(&self, path: impl Into<PathBuf>) -> Handle<A> {
        self.new_assets.load(path.into())
    }

    /// Schedule an asset for loading using the specified loader
    pub fn load_with<A: Asset, L: AssetLoader<A>>(
        &self,
        loader: L,
        path: impl Into<PathBuf>,
    ) -> Handle<A> {
        self.new_assets.load_with(loader, path)
    }

    /// Remove dead assets of a specific type only, calling the `handler` function
    /// on every deleted asset afterwards
    pub fn cleanup_with<A: Asset, F: FnMut(Id<A>, &WeakHandle<A>, &mut A)>(&mut self, handler: F) {
        self.storage_mut().cleanup_with(handler);
    }

    /// Get immutable iterator over assets of a specific type
    pub fn iter<A: Asset>(&self) -> impl Iterator<Item = (Handle<A>, &A)> + '_ {
        self.storage().iter()
    }

    /// Get mutable iterator over assets of a specific type
    pub fn iter_mut<A: Asset>(&mut self) -> impl Iterator<Item = (Handle<A>, &mut A)> + '_ {
        self.storage_mut().iter_mut()
    }

    /// Make loaded assets available for use
    pub fn flush(&mut self) {
        log::debug!("Flushing assets");

        for loaded in self.loaded_rx.try_iter() {
            match self.storages.get_mut_dyn(loaded.type_id) {
                Some(storage) => unsafe { storage.flush(loaded) },
                None => panic!("asset is not registered"),
            }
        }
    }

    /// Start loading scheduled assets in background threads
    pub fn spawn_workers(&mut self) -> Waiter {
        for _ in 0..num_cpus::get() {
            let new_assets = self.new_assets.clone();
            let source = self.source.clone();
            let sched_rx = self.sched_rx.clone();
            let loaded_tx = self.new_assets.loaded_tx.clone();
            let waiter = self.new_assets.waiter.clone();
            let counter = self.new_assets.counter.clone();
            thread::spawn(move || {
                worker_thread(new_assets, source, sched_rx, loaded_tx, waiter, counter);
            });
        }

        self.new_assets.waiter.clone()
    }

    /// Lock a progress counter
    pub fn progress_counter(&self) -> impl ops::DerefMut<Target = ProgressCounter> + '_ {
        self.new_assets.counter.lock().unwrap()
    }
}

impl<A: Asset> ops::Index<&Handle<A>> for Assets {
    type Output = A;
    fn index(&self, handle: &Handle<A>) -> &A {
        self.get(handle).unwrap()
    }
}

impl<A: Asset> ops::IndexMut<&Handle<A>> for Assets {
    fn index_mut(&mut self, handle: &Handle<A>) -> &mut A {
        self.get_mut(handle).unwrap()
    }
}

/// Homogeneous asset storage
struct AssetStorage<A: Asset> {
    assets: FxHashMap<Id<A>, (WeakHandle<A>, A)>,
}

fn storage_id<A: Asset>() -> TypeId {
    TypeId::of::<AssetStorage<A>>()
}

impl<A: Asset> AssetStorage<A> {
    fn new() -> AssetStorage<A> {
        AssetStorage {
            assets: FxHashMap::default(),
        }
    }

    /// Get an immutable reference to asset by its ID, if the asset exists
    fn get_by_id(&self, id: Id<A>) -> Option<&A> {
        self.assets.get(&id).map(|(_, asset)| asset)
    }

    /// Get a mutable reference to asset by its ID, if the asset exists
    fn get_by_id_mut(&mut self, id: Id<A>) -> Option<&mut A> {
        self.assets.get_mut(&id).map(|(_, asset)| asset)
    }

    /// Get an immutable reference to an asset by handle to it. Panics if the handle came from a
    /// different asset storage.
    fn get(&self, handle: &Handle<A>) -> Option<&A> {
        self.get_by_id(handle.id())
    }

    /// Get a mutable reference to an asset by handle to it
    fn get_mut(&mut self, handle: &Handle<A>) -> Option<&mut A> {
        self.get_by_id_mut(handle.id())
    }

    /// Insert an asset returning handle to it
    fn insert(&mut self, asset: A, handle: &Handle<A>) {
        let weak = handle.downgrade();
        self.assets.insert(handle.id(), (weak, asset));
    }

    /// Remove assets with zero live handles, calling the `handler` function on every deleted asset
    /// afterwards
    fn cleanup_with<F: FnMut(Id<A>, &WeakHandle<A>, &mut A)>(&mut self, mut handler: F) {
        let mut count = 0;
        self.assets.retain(|&id, (weak, asset)| {
            if weak.is_dead() {
                count += 1;
                handler(id, weak, asset);
                false
            } else {
                true
            }
        });
        if count > 0 {
            log::debug!("Removed {} assets of type {}", count, type_name::<A>());
        }
    }

    /// Get immutable iterator over assets
    fn iter(&self) -> impl Iterator<Item = (Handle<A>, &A)> + '_ {
        self.assets
            .values()
            .flat_map(|(weak, asset)| weak.upgrade().map(|handle| (handle, asset)))
    }

    /// Get mutable iterator over assets
    fn iter_mut(&mut self) -> impl Iterator<Item = (Handle<A>, &mut A)> + '_ {
        self.assets
            .values_mut()
            .flat_map(|(weak, asset)| weak.upgrade().map(|handle| (handle, asset)))
    }
}

impl<A: Asset> ops::Index<&Handle<A>> for AssetStorage<A> {
    type Output = A;
    fn index(&self, handle: &Handle<A>) -> &A {
        self.get(handle).unwrap()
    }
}

impl<A: Asset> ops::IndexMut<&Handle<A>> for AssetStorage<A> {
    fn index_mut(&mut self, handle: &Handle<A>) -> &mut A {
        self.get_mut(handle).unwrap()
    }
}

trait DynStorage: Send + Sync + 'static {
    fn cleanup(&mut self);
    unsafe fn flush(&mut self, loaded: LoadedAsset);
}

impl_any!(DynStorage);

impl<A: Asset> DynStorage for AssetStorage<A> {
    fn cleanup(&mut self) {
        self.cleanup_with(|_, _, _| ());
    }

    unsafe fn flush(&mut self, loaded: LoadedAsset) {
        let (weak, asset) = loaded.unpack();
        if let Some(handle) = weak.upgrade() {
            self.assets.insert(handle.id(), (weak, asset));
        }
    }
}

/// Used to add new assets inside loader
pub struct NewAssets {
    handle_allocator: Arc<HandleAllocator>,
    handles: Arc<HandleMap>,
    sched_tx: Sender<SchedAsset>,
    loaded_tx: Sender<LoadedAsset>,
    waiter: Waiter,
    counter: Arc<Mutex<ProgressCounter>>,
}

impl NewAssets {
    /// Schedule an asset for loading using the default loader
    pub fn load<A: AssetDefaultLoader>(&self, path: impl Into<PathBuf>) -> Handle<A> {
        self.load_with(A::Loader::default(), path)
    }

    /// Schedule an asset for loading using the specified loader
    pub fn load_with<A: Asset, L: AssetLoader<A>>(
        &self,
        loader: L,
        path: impl Into<PathBuf>,
    ) -> Handle<A> {
        let path = path.into();
        if let Some(handle) = self.handles.get(&path) {
            handle
        } else {
            self.waiter.increment_count();
            self.counter.lock().unwrap().schedule_loading();

            let handle = self.handle_allocator.alloc_handle();
            self.handles.insert(path.clone(), &handle);

            let sched = SchedAsset::new(handle.downgrade(), path, loader);
            let _ = self.sched_tx.send(sched);

            handle
        }
    }

    /// Insert a preloaded asset
    pub fn insert<A: Asset>(&self, asset: A) -> Handle<A> {
        let handle = self.handle_allocator.alloc_handle();
        let loaded = LoadedAsset::new(handle.downgrade(), asset);
        let _ = self.loaded_tx.send(loaded);
        handle
    }
}

type AssetFactory = Box<
    dyn (FnOnce(&NewAssets, &dyn AssetSource, &Path) -> Result<Box<dyn Any + Send + Sync>>)
        + Send
        + Sync,
>;

/// Asset to be loaded
struct SchedAsset {
    type_id: TypeId,
    path: PathBuf,
    weak: WeakHandle<()>,
    factory: AssetFactory,
}

impl SchedAsset {
    fn new<A: Asset, L: AssetLoader<A>>(
        weak: WeakHandle<A>,
        path: PathBuf,
        loader: L,
    ) -> SchedAsset {
        let factory: AssetFactory = Box::new(move |assets, source, path| {
            loader.load(assets, source, path).map(|v| {
                let v: Box<dyn Any + Send + Sync> = Box::new(v);
                v
            })
        });

        SchedAsset {
            type_id: storage_id::<A>(),
            path,
            weak: unsafe { mem::transmute(weak) },
            factory,
        }
    }

    fn load(
        self,
        new_assets: &NewAssets,
        source: &dyn AssetSource,
    ) -> (PathBuf, Result<LoadedAsset>) {
        let type_id = self.type_id;
        let weak = self.weak;
        let res = (self.factory)(new_assets, source, &self.path).map(|data| LoadedAsset {
            type_id,
            weak,
            data,
        });
        (self.path, res)
    }
}

/// Loaded, but not inserted asset
struct LoadedAsset {
    type_id: TypeId,
    weak: WeakHandle<()>,
    data: Box<dyn Any + Send + Sync>,
}

impl LoadedAsset {
    fn new<A: Asset>(handle: WeakHandle<A>, asset: A) -> LoadedAsset {
        LoadedAsset {
            type_id: storage_id::<A>(),
            weak: unsafe { mem::transmute(handle) },
            data: Box::new(asset),
        }
    }

    /// # Safety
    /// Asset type must be the same as used during construction
    unsafe fn unpack<A: Asset>(self) -> (WeakHandle<A>, A) {
        let weak = mem::transmute(self.weak);
        let asset = self.data.downcast_unchecked();
        (weak, *asset)
    }
}

/// Stores (path, handle) pairs
struct HandleMap {
    handles: DashMap<(PathBuf, TypeId), WeakHandle<()>, FxBuildHasher>,
}

impl HandleMap {
    fn new() -> HandleMap {
        HandleMap {
            handles: DashMap::with_hasher(FxBuildHasher::default()),
        }
    }

    fn get<A: Asset>(&self, path: &PathBuf) -> Option<Handle<A>> {
        // HACK: no other way to avoid cloning the path
        let path = unsafe { ptr::read(path) };
        let key = ManuallyDrop::new((path, storage_id::<A>()));

        self.handles
            .get(&key)
            .and_then(|w| w.upgrade())
            .and_then(|h| unsafe { mem::transmute(h) })
    }

    fn insert<A: Asset>(&self, path: PathBuf, handle: &Handle<A>) {
        let weak = handle.downgrade();
        let weak_erased = unsafe { mem::transmute(weak) };
        self.handles.insert((path, storage_id::<A>()), weak_erased);
    }

    fn cleanup(&self) {
        self.handles.retain(|_, weak| !weak.is_dead());
    }
}

fn worker_thread(
    new_assets: Arc<NewAssets>,
    source: Arc<dyn AssetSource>,
    sched_rx: Receiver<SchedAsset>,
    loaded_tx: Sender<LoadedAsset>,
    waiter: Waiter,
    counter: Arc<Mutex<ProgressCounter>>,
) {
    loop {
        let sched = sched_rx.recv().unwrap();
        counter.lock().unwrap().start_loading(sched.path.clone());
        let (path, res) = sched.load(&*new_assets, &*source);
        match res {
            Ok(v) => loaded_tx.send(v).unwrap(),
            Err(e) => {
                counter.lock().unwrap().add_error(path.clone(), e);
            }
        }
        counter.lock().unwrap().finish_loading(&path);
        waiter.decrement_count();
    }
}

/// Used to block thread until all assets are loaded
#[derive(Clone)]
pub struct Waiter {
    inner: Arc<WaiterInner>,
}

struct WaiterInner {
    condvar: Condvar,
    count: Mutex<usize>,
}

#[allow(clippy::mutex_atomic)] // clippy, are you stupid or what? i need a mutex to use condvar
impl Waiter {
    fn new() -> Waiter {
        Waiter {
            inner: Arc::new(WaiterInner {
                condvar: Condvar::new(),
                count: Mutex::new(0),
            }),
        }
    }

    fn increment_count(&self) {
        let mut guard = self.inner.count.lock().unwrap();
        *guard += 1;
    }

    fn decrement_count(&self) {
        let mut guard = self.inner.count.lock().unwrap();
        *guard -= 1;
        if *guard == 0 {
            self.inner.condvar.notify_all();
        }
    }

    /// Wait until all assets are loaded
    pub fn wait(&self) {
        let guard = self.inner.count.lock().unwrap();
        let res = self.inner.condvar.wait_while(guard, |count| *count > 0);
        let _guard = res.unwrap();
    }

    /// Wait until all assets are loaded, timing out after a specified duration. Returns `true`, if
    /// the wait timed out
    pub fn wait_timeout(&self, dur: Duration) -> bool {
        let guard = self.inner.count.lock().unwrap();
        let condvar = &self.inner.condvar;
        let res = condvar.wait_timeout_while(guard, dur, |count| *count > 0);
        res.unwrap().1.timed_out()
    }
}
