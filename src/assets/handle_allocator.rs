use std::sync::atomic::{AtomicUsize, Ordering};

use super::{Asset, Handle, Id};

/// Asset handle allocator, implemented as a monotonically increasing counter
#[derive(Debug)]
pub struct HandleAllocator {
    counter: AtomicUsize,
}

impl Default for HandleAllocator {
    fn default() -> HandleAllocator {
        HandleAllocator::new()
    }
}

impl HandleAllocator {
    /// Create a new handle allocator
    pub fn new() -> HandleAllocator {
        HandleAllocator {
            counter: AtomicUsize::new(0),
        }
    }

    fn alloc_id(&self) -> usize {
        self.counter.fetch_add(1, Ordering::Relaxed)
    }

    /// Allocate a new handle
    pub fn alloc_handle<A: Asset>(&self) -> Handle<A> {
        Handle::new(Id::new(self.alloc_id()))
    }
}
