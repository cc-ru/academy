use anyhow::Error;
use fxhash::FxHashSet;

use std::path::PathBuf;

/// Progress counter
#[derive(Default, Debug)]
pub struct ProgressCounter {
    total: usize,
    done: usize,
    loading: FxHashSet<PathBuf>,
    errors: Vec<(PathBuf, Error)>,
}

impl ProgressCounter {
    /// Create a new progress counter
    pub fn new() -> ProgressCounter {
        Default::default()
    }

    /// Schedule loading
    pub fn schedule_loading(&mut self) {
        self.total += 1;
    }

    /// Start loading a file
    pub fn start_loading(&mut self, path: PathBuf) {
        self.loading.insert(path);
    }

    /// Finish loading a file
    pub fn finish_loading(&mut self, path: &PathBuf) {
        self.done += 1;
        self.loading.remove(path);
    }

    /// Add an error
    pub fn add_error(&mut self, path: PathBuf, error: Error) {
        self.errors.push((path, error));
    }
}
