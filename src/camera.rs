//! Camera

use glam::Vec2;

use crate::graphics::Encoder;

/// Top-down camera
#[derive(Clone, Copy, Debug)]
pub struct Camera {
    /// Point to look at
    pub center: Vec2,
    /// Scale
    pub scale: f32,
}

impl Default for Camera {
    fn default() -> Camera {
        Camera {
            center: Vec2::zero(),
            scale: 1.0,
        }
    }
}

impl Camera {
    /// Apply camera transformation
    pub fn apply(&self, encoder: &mut Encoder<'_>, res: Vec2) {
        encoder.translate(-self.center * 32.0);
        encoder.scale([self.scale, self.scale]);
        encoder.translate(res / 2.0);
    }
}
