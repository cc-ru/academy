use std::path::PathBuf;

use anyhow::Result;
use fxhash::FxHashMap;
use glam::Vec2;
use glium::glutin::event::{Event, WindowEvent};
use glium::glutin::event_loop::{ControlFlow, EventLoop};
use glium::glutin::window::WindowBuilder;
use legion::{Resources, Schedule, World};

use academy::assets::{Assets, DirectorySource, Handle};
use academy::audio::{flush_sounds_system, Audio, Sound};
use academy::camera::Camera;
use academy::components::{CollisionRect, Position, SpriteOrigin, Velocity};
use academy::graphics::{DeltaTime, DrawList, Encoder, Font, Graphics, Sprite, WindowResolution};
use academy::input::{Controls, Input};
use academy::map::{ChunkPosition, Map, PrefabList};
use academy::style;
use academy::systems::particle::{
    accelerate_particles_system, emit_particles_system, kill_old_particles_system,
    move_particles_system, spin_particles_system, update_particle_color_system,
    update_particle_size_system, ParticleEmitter, ParticleSystemParams,
};
use academy::systems::{
    camera_scale_control_system, center_on_player_system, collision_system, debug_map_bvh_system,
    draw_background_system, draw_foreground_system, manage_chunks_system, player_controls_system,
    step_sounds_system, Character, PlayerCharacter,
};
use academy::ui::widget::{Geometry, PaddingBox, Widget};
use academy::ui::{Container, StyleChain, UiContext, UiStorage, WidgetId};
use academy::util::{Aabr, SideOffsets, Stats, SystemProfile};

fn main() -> Result<()> {
    let mut assets = Assets::new(DirectorySource::new("./assets"));
    let event_loop = EventLoop::new();
    let window_config = WindowBuilder::new();
    let mut graphics = Graphics::new(window_config, &event_loop)?;

    assets.register::<Font>();
    assets.register::<Sprite>();
    assets.register::<Sound>();
    assets.register::<ParticleSystemParams>();

    let font = assets.load("fonts/OpenSans-Regular.ttf");
    let particle_params: Handle<ParticleSystemParams> = assets.load("particles/test.json");

    let mut prefab_list = PrefabList::open("assets/prefabs/base.bin")?;
    prefab_list.resolve_assets(&assets);
    let mut prefab_lists = FxHashMap::default();
    prefab_lists.insert(PathBuf::from("base.bin"), prefab_list);

    let mut map = Map::open("assets/maps/map.bin")?;

    let mut world = World::default();
    map.load_chunk(ChunkPosition::new(0, 0), &prefab_lists, &mut world, &assets)?;

    let sprite: Handle<Sprite> = assets.load("sprites/wizard-idle.png");
    world.push((
        Position(Vec2::new(5.0, 5.0)),
        PlayerCharacter,
        Velocity(Vec2::new(0.0, 0.0)),
        Character {
            speed: 12.0,
            last_step: None,
            step_sounds: (0..9)
                .map(|i| assets.load(format!("sounds/step-{:02}.ogg", i)))
                .collect(),
        },
        sprite,
        SpriteOrigin(Vec2::new(2.0, 7.2)),
        CollisionRect(Aabr::new(Vec2::new(-1.0, -0.5), Vec2::new(1.1, 0.7))),
    ));

    let mut resources = Resources::default();
    resources.insert(assets);
    resources.insert(DrawList::default());
    resources.insert(Camera::default());
    resources.insert(map);
    resources.insert(prefab_lists);
    resources.insert(Audio::new()?);
    resources.insert(UiStorage::default());

    let controls = Controls::from_file("assets/controls.ron")?;
    let input = Input::new(controls);
    resources.insert(input);

    let mut updating = Schedule::builder()
        .add_system(camera_scale_control_system(1.0))
        .add_system(player_controls_system())
        .add_system(SystemProfile::new(
            "particles-emit",
            emit_particles_system(),
        ))
        .add_system(SystemProfile::new(
            "particles-kill",
            kill_old_particles_system(),
        ))
        .add_system(SystemProfile::new(
            "particles-accelerate",
            accelerate_particles_system(),
        ))
        .add_system(SystemProfile::new(
            "particles-updsize",
            update_particle_size_system(),
        ))
        .add_system(SystemProfile::new(
            "particles-updcol",
            update_particle_color_system(),
        ))
        .add_system(SystemProfile::new(
            "particles-spin",
            spin_particles_system(),
        ))
        .add_system(move_particles_system())
        .add_system(SystemProfile::new("collision", collision_system()))
        .add_system(center_on_player_system())
        .add_system(step_sounds_system())
        .add_system(SystemProfile::new("draw-bg", draw_background_system()))
        .add_system(SystemProfile::new(
            "draw-fg",
            draw_foreground_system(Vec::new()),
        ))
        .add_system(debug_map_bvh_system(false))
        .add_thread_local_fn(manage_chunks_system)
        .add_system(flush_sounds_system())
        .build();

    let mut assets = resources.get_mut::<Assets>().unwrap();
    let waiter = assets.spawn_workers();
    waiter.wait();
    assets.flush();
    let params = assets[&particle_params].clone();
    let emitter = ParticleEmitter::new(&assets, params);
    drop(assets);

    world.push((emitter, Position(Vec2::new(7.0, 1.0))));

    let mut instant = std::time::Instant::now();

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            *control_flow = ControlFlow::Exit;
        }

        Event::WindowEvent { event, .. } => {
            let mut input = resources.get_mut::<Input>().unwrap();
            input.handle_event(&event);
        }

        Event::MainEventsCleared => {
            let dt = instant.elapsed();
            Stats::get().add_time("SPF", dt);
            let dt = dt.as_secs_f32();
            instant = std::time::Instant::now();
            resources.insert(DeltaTime(dt));

            let resolution = graphics.resolution();
            resources.insert(WindowResolution(resolution));

            updating.execute(&mut world, &mut resources);

            let mut drawlist = resources.get_mut::<DrawList>().unwrap();
            let mut assets = resources.get_mut::<Assets>().unwrap();
            let mut storage = resources.get_mut::<UiStorage>().unwrap();
            let mut input = resources.get_mut::<Input>().unwrap();

            waiter.wait();
            assets.flush();

            let mut encoder = Encoder::new(&mut drawlist);

            let mut uctx = UiContext {
                encoder: &mut encoder,
                assets: &assets,
                font: font.id(),
                id: WidgetId::new(),
                storage: &mut storage,
                input: &input,
                dt,
                style: StyleChain::default(),
            };

            let style = style! {
                text {
                    font_size: 16.0,
                };
            };

            let mut stack = PaddingBox::new(&mut uctx, SideOffsets::new_equal(4.0), |stack| {
                stack.style_scope(&style, |scope| {
                    scope.text(&Stats::get().to_string());
                    scope.button("click me", || std::process::exit(0));
                });
            });

            let size = stack.size(&uctx, resolution);

            let bounds = Aabr::new(Vec2::zero(), size);
            let geometry = Geometry {
                bounds,
                clamped_bounds: bounds,
                parent_clamped_bounds: bounds,
            };

            stack.update(&mut uctx, &geometry);
            stack.draw(&mut uctx, &geometry);

            drop(stack);

            if let Err(e) = graphics.render(&mut drawlist, &mut assets) {
                eprintln!("Rendering error: {:?}", e);
                *control_flow = ControlFlow::Exit;
            }

            input.reset();
        }

        _ => {}
    });
}
