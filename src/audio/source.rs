use std::sync::Arc;
use std::time::Duration;

use rodio::Source;

/// Reference counted buffer of samples
#[derive(Clone, Debug)]
pub struct SharedBuffer {
    data: Arc<Vec<i16>>,
    position: usize,
    channels: u16,
    sample_rate: u32,
    duration: Duration,
}

impl SharedBuffer {
    /// Create a new shared audio buffer
    pub fn new(channels: u16, sample_rate: u32, data: Arc<Vec<i16>>) -> SharedBuffer {
        let ns_in_s = 1_000_000_000;
        let duration_s = (data.len() as u64) / (sample_rate as u64) / (channels as u64);
        let duration_ns =
            ns_in_s * (data.len() as u64) / (sample_rate as u64) / (channels as u64) % ns_in_s;
        let duration = Duration::new(duration_s, duration_ns as u32);

        SharedBuffer {
            data,
            position: 0,
            channels,
            sample_rate,
            duration,
        }
    }
}

impl Iterator for SharedBuffer {
    type Item = i16;

    fn next(&mut self) -> Option<i16> {
        let &sample = self.data.get(self.position)?;
        self.position += 1;
        Some(sample)
    }
}

impl Source for SharedBuffer {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<Duration> {
        Some(self.duration)
    }
}
