use std::sync::Arc;

use anyhow::Result;
use rodio::{Decoder, Source};

use super::SharedBuffer;
use crate::assets::{Asset, AssetDefaultLoader, AssetReader, SimpleAssetLoader};

/// Sound asset
pub struct Sound {
    /// Audio data
    pub source: SharedBuffer,
}

impl Asset for Sound {}

impl AssetDefaultLoader for Sound {
    type Loader = SoundLoader;
}

/// Sound loader
#[derive(Default)]
pub struct SoundLoader;

impl SimpleAssetLoader<Sound> for SoundLoader {
    fn load(&self, reader: impl AssetReader) -> Result<Sound> {
        let decoder = Decoder::new(reader)?;
        let sample_rate = decoder.sample_rate();
        let channels = decoder.channels();
        let data = decoder.collect::<Vec<_>>();
        let source = SharedBuffer::new(channels, sample_rate, Arc::new(data));
        Ok(Sound { source })
    }
}
