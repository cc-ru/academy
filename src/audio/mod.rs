//! Audio subsystem

#![allow(missing_docs)]

mod format;
mod source;

pub use self::format::{Sound, SoundLoader};
pub use self::source::SharedBuffer;

use std::sync::Arc;
use std::thread;

use anyhow::{Context, Result};
use crossbeam::queue::SegQueue;
use glam::Vec3;
use legion::system;
use rodio::dynamic_mixer::{mixer, DynamicMixerController};
use rodio::Sink;

use crate::assets::{Assets, Id};

/// Audio manager
#[allow(dead_code)] // sink doesn't work if it gets dropped
pub struct Audio {
    mixer: Arc<DynamicMixerController<i16>>,
    scheduled_spatial: SegQueue<(Id<Sound>, Vec3, f32)>,
    scheduled_nonspatial: SegQueue<(Id<Sound>, f32)>,
    left_ear: Vec3,
    right_ear: Vec3,
}

impl Audio {
    /// Create a new audio manager
    pub fn new() -> Result<Audio> {
        let (controller, source) = mixer(2, 44100);

        thread::spawn(move || {
            let device = rodio::default_output_device()
                .context("Cannot open audio device")
                .unwrap();
            let sink = Sink::new(&device);
            sink.append(source);
            sink.play();
            sink.sleep_until_end();
        });

        controller.add(rodio::source::Zero::new(2, 44100));

        Ok(Audio {
            mixer: controller,
            scheduled_spatial: SegQueue::new(),
            scheduled_nonspatial: SegQueue::new(),
            left_ear: Vec3::zero(),
            right_ear: Vec3::zero(),
        })
    }

    /// Set left and right ear positions. LEFT SHOULD NOT BE EQUAL TO RIGHT!111111
    pub fn set_ear_pos(&mut self, left: Vec3, right: Vec3) {
        self.left_ear = left;
        self.right_ear = right;
    }

    /// Play a sound with given volume (not spatial)
    pub fn play_sound(&self, sound: impl Into<Id<Sound>>, volume: f32) {
        self.scheduled_nonspatial.push((sound.into(), volume));
    }

    /// Play a spatial sound at given coordinates
    pub fn play_sound_at(&self, sound: impl Into<Id<Sound>>, pos: Vec3, volume: f32) {
        self.scheduled_spatial.push((sound.into(), pos, volume));
    }

    /// Flush sounds
    pub fn flush(&mut self, assets: &Assets) {
        while let Ok((sound, pos, volume)) = self.scheduled_spatial.pop() {
            let source = assets.get_by_id(sound).unwrap().source.clone();
            self.mixer.add(rodio::source::Spatial::new(
                rodio::source::ChannelVolume::new(source, vec![volume, volume]),
                pos.into(),
                self.left_ear.into(),
                self.right_ear.into(),
            ));
        }

        while let Ok((sound, volume)) = self.scheduled_nonspatial.pop() {
            let source = assets.get_by_id(sound).unwrap().source.clone();
            self.mixer.add(rodio::source::ChannelVolume::new(
                source,
                vec![volume, volume],
            ));
        }
    }
}

#[system]
pub fn flush_sounds(#[resource] audio: &mut Audio, #[resource] assets: &Assets) {
    audio.flush(assets);
}
