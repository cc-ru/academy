//! Top-down RPG with magic

#![warn(missing_docs)]
#![warn(rust_2018_idioms)]

#[macro_use]
pub mod util;
#[macro_use]
pub mod ui;
pub mod assets;
pub mod audio;
pub mod camera;
pub mod components;
pub mod graphics;
pub mod input;
pub mod map;
pub mod systems;
