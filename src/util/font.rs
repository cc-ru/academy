use glam::Vec2;
use rusttype::{Font, Point, Scale, ScaledGlyph};

use crate::util::Aabr;

/// Font utilities
pub trait FontUtil {
    /// Get reference to the font
    fn font(&self) -> &Font<'_>;

    /// Calculate width of the given text
    fn text_width(&self, size: f32, text: &str) -> f32 {
        let font = self.font();
        let scale = Scale::uniform(size);
        let glyphs = font.glyphs_for(text.chars());

        let mut last: Option<ScaledGlyph<'_>> = None;
        let mut width = 0.0;
        for glyph in glyphs {
            let glyph = glyph.scaled(scale);
            if let Some(last) = last {
                width += font.pair_kerning(scale, last.id(), glyph.id());
            }
            width += glyph.h_metrics().advance_width;
            last = Some(glyph);
        }

        width
    }

    /// Calculate AABR of the given text
    fn text_aabr(&self, pos: Vec2, size: f32, text: &str) -> Option<Aabr> {
        let font = self.font();

        let pos = Point {
            x: pos.x(),
            y: pos.y(),
        };
        let glyphs = font.layout(&text, Scale::uniform(size), pos);

        let mut aabr: Option<Aabr> = None;

        for glyph in glyphs {
            let bb = match glyph.pixel_bounding_box() {
                Some(v) => v,
                None => continue,
            };
            let glyph_aabr = Aabr {
                min: Vec2::new(bb.min.x as f32, bb.min.y as f32),
                max: Vec2::new(bb.max.x as f32, bb.max.y as f32),
            };
            aabr = Some(aabr.map(|v| v.union(&glyph_aabr)).unwrap_or(glyph_aabr));
        }

        aabr
    }
}

impl FontUtil for Font<'_> {
    fn font(&self) -> &Font<'_> {
        self
    }
}
