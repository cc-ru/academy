use std::fmt::{self, Display};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};

use dashmap::DashMap;
use fxhash::FxBuildHasher;
use legion::storage::ComponentTypeId;
use legion::systems::{CommandBuffer, ResourceTypeId, Runnable, SystemId, UnsafeResources};
use legion::world::{ArchetypeAccess, World, WorldId};
use once_cell::sync::Lazy;

#[derive(Debug)]
struct AtomicDuration {
    secs: AtomicU32,
    nanos: AtomicU32,
}

impl AtomicDuration {
    fn new(dur: Duration) -> AtomicDuration {
        let (secs, nanos) = (dur.as_secs() as u32, dur.subsec_nanos());
        AtomicDuration {
            secs: AtomicU32::new(secs),
            nanos: AtomicU32::new(nanos),
        }
    }

    fn load(&self) -> Duration {
        Duration::new(
            self.secs.load(Ordering::SeqCst) as _,
            self.nanos.load(Ordering::SeqCst) as _,
        )
    }
}

#[derive(Debug)]
struct TimeEntry {
    mean: AtomicDuration,
}

#[derive(Debug)]
struct CountEntry {
    count: AtomicU32,
}

/// Lock free time & count statistics storage
#[derive(Debug)]
pub struct Stats {
    time_entries: DashMap<String, TimeEntry, FxBuildHasher>,
    count_entries: DashMap<String, CountEntry, FxBuildHasher>,
}

impl Default for Stats {
    fn default() -> Stats {
        Stats {
            time_entries: DashMap::with_hasher(FxBuildHasher::default()),
            count_entries: DashMap::with_hasher(FxBuildHasher::default()),
        }
    }
}

static GLOBAL_STATS: Lazy<Arc<Stats>> = Lazy::new(|| Arc::new(Stats::new()));

impl Stats {
    /// Create a new stats instance
    pub fn new() -> Stats {
        Stats::default()
    }

    /// Get global stats instance
    pub fn get() -> &'static Arc<Stats> {
        &GLOBAL_STATS
    }

    /// Create a new time measurement guard
    pub fn time_guard<'a>(&'a self, name: &'a str) -> TimeGuard<'a> {
        TimeGuard {
            name,
            stats: self,
            instant: Instant::now(),
        }
    }

    /// Set count to a new value
    pub fn set_count(&self, name: &str, value: u32) {
        if let Some(entry) = self.count_entries.get(name) {
            entry.count.store(value, Ordering::SeqCst);
        } else {
            self.count_entries.insert(
                name.to_owned(),
                CountEntry {
                    count: AtomicU32::new(value),
                },
            );
        }
    }

    /// Add to count
    pub fn add_count(&self, name: &str, value: u32) {
        if let Some(entry) = self.count_entries.get(name) {
            entry.count.fetch_add(value, Ordering::SeqCst);
        } else {
            self.count_entries.insert(
                name.to_owned(),
                CountEntry {
                    count: AtomicU32::new(value),
                },
            );
        }
    }

    /// Add a time measurement
    pub fn add_time(&self, name: &str, time: Duration) {
        if let Some(entry) = self.time_entries.get(name) {
            let smoothing = 0.99;
            let mean = entry.mean.load().mul_f64(smoothing) + time.mul_f64(1.0 - smoothing);
            let (secs, nanos) = (mean.as_secs() as u32, mean.subsec_nanos());
            entry.mean.secs.store(secs, Ordering::SeqCst);
            entry.mean.nanos.store(nanos, Ordering::SeqCst);
        } else {
            self.time_entries.insert(
                name.to_owned(),
                TimeEntry {
                    mean: AtomicDuration::new(time),
                },
            );
        }
    }

    /// Reset time measurements
    pub fn reset_time(&self, name: &str) {
        self.time_entries.remove(name);
    }
}

struct FmtDuration(Duration);

impl Display for FmtDuration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let v = self.0.as_secs_f64();
        write!(f, "{:.4}ms", v * 1e3)
    }
}

impl Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut counts = self
            .count_entries
            .iter()
            .map(|el| {
                let val = format!("{}", el.value().count.load(Ordering::SeqCst));
                (el.key().to_owned(), val)
            })
            .collect::<Vec<_>>();

        let mut times = self
            .time_entries
            .iter()
            .map(|el| {
                let mean = el.value().mean.load();
                let val = format!("{}", FmtDuration(mean));
                (el.key().to_owned(), val)
            })
            .collect::<Vec<_>>();

        counts.sort_by(|a, b| a.0.cmp(&b.0));
        times.sort_by(|a, b| a.0.cmp(&b.0));

        let mut first = true;
        for (k, v) in counts.into_iter().chain(times) {
            if first {
                first = false;
            } else {
                writeln!(f)?;
            }
            write!(f, "{}: {}", k, v)?;
        }

        Ok(())
    }
}

/// Stores the time measurement on Drop
pub struct TimeGuard<'a> {
    name: &'a str,
    stats: &'a Stats,
    instant: Instant,
}

impl Drop for TimeGuard<'_> {
    fn drop(&mut self) {
        self.stats.add_time(self.name, self.instant.elapsed());
    }
}

/// Wraps a legion system providing time measurements
pub struct SystemProfile<R: Runnable> {
    /// Inner system
    pub system: R,
    name: &'static str,
}

impl<R: Runnable> SystemProfile<R> {
    /// Wrap a system
    pub fn new(name: &'static str, system: R) -> SystemProfile<R> {
        SystemProfile { system, name }
    }
}

impl<R: Runnable> Runnable for SystemProfile<R> {
    fn name(&self) -> Option<&SystemId> {
        self.system.name()
    }

    fn reads(&self) -> (&[ResourceTypeId], &[ComponentTypeId]) {
        self.system.reads()
    }

    fn writes(&self) -> (&[ResourceTypeId], &[ComponentTypeId]) {
        self.system.writes()
    }

    fn prepare(&mut self, world: &World) {
        self.system.prepare(world)
    }

    fn accesses_archetypes(&self) -> &ArchetypeAccess {
        self.system.accesses_archetypes()
    }

    unsafe fn run_unsafe(&mut self, world: &World, resources: &UnsafeResources) {
        let _guard = Stats::get().time_guard(self.name);
        self.system.run_unsafe(world, resources)
    }

    fn command_buffer_mut(&mut self, world: WorldId) -> Option<&mut CommandBuffer> {
        self.system.command_buffer_mut(world)
    }
}
