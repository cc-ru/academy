use glam::Vec2;

/// Orientation (horizontal or vertical)
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Orientation {
    /// Vertical orientation (Y axis)
    Vertical,
    /// Horizontal orientation (X axis)
    Horizontal,
}

impl Orientation {
    /// Get orientation as a vector (left-to-right, top-to-bottom, normalized)
    pub fn as_vector(self) -> Vec2 {
        match self {
            Orientation::Vertical => Vec2::new(0.0, 1.0),
            Orientation::Horizontal => Vec2::new(1.0, 0.0),
        }
    }

    /// Get orientation orthogonal to `self`
    pub fn orthogonal(self) -> Orientation {
        match self {
            Orientation::Vertical => Orientation::Horizontal,
            Orientation::Horizontal => Orientation::Vertical,
        }
    }
}
