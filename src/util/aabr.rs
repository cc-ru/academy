use std::ops;

use glam::{Mat3, Vec2};

/// Axis aligned bounding rectangle
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Aabr {
    /// The top-left corner
    pub min: Vec2,
    /// The bottom-right corner
    pub max: Vec2,
}

impl Aabr {
    /// Create a new AABR
    pub fn new(min: Vec2, max: Vec2) -> Aabr {
        Aabr { min, max }
    }

    /// Create a new AABR
    pub fn from_pos_extent(pos: Vec2, extent: Vec2) -> Aabr {
        Aabr {
            min: pos,
            max: pos + extent,
        }
    }

    /// Get width
    pub fn width(&self) -> f32 {
        self.extent().x()
    }

    /// Get height
    pub fn height(&self) -> f32 {
        self.extent().y()
    }

    /// Get extent vector (`max - min`)
    pub fn extent(&self) -> Vec2 {
        self.max - self.min
    }

    /// Calculate union with other AABR
    pub fn union(&self, other: &Aabr) -> Aabr {
        Aabr {
            min: self.min.min(other.min),
            max: self.max.max(other.max),
        }
    }

    /// Calculate intersection with other AABR
    pub fn intersection(&self, other: &Aabr) -> Aabr {
        Aabr {
            min: self.min.max(other.min),
            max: self.max.min(other.max),
        }
    }

    /// Get top left corner
    pub fn top_left(&self) -> Vec2 {
        self.min
    }

    /// Get top right corner
    pub fn top_right(&self) -> Vec2 {
        self.min + Vec2::new(self.width(), 0.0)
    }

    /// Get bottom right corner
    pub fn bottom_right(&self) -> Vec2 {
        self.max
    }

    /// Get bottom left corner
    pub fn bottom_left(&self) -> Vec2 {
        self.min + Vec2::new(0.0, self.height())
    }

    /// Translate AABR by a vector
    pub fn translate(&self, dir: Vec2) -> Aabr {
        Aabr::new(self.min + dir, self.max + dir)
    }

    /// Transform AABR by a given matrix
    pub fn transform(&self, matrix: &Mat3) -> Aabr {
        let points = [
            matrix.transform_point2(self.top_left()),
            matrix.transform_point2(self.top_right()),
            matrix.transform_point2(self.bottom_left()),
            matrix.transform_point2(self.bottom_right()),
        ];

        let (min, max) = points
            .iter()
            .fold((points[0], points[3]), |(min, max), &b| {
                (min.min(b), max.max(b))
            });

        Aabr::new(min, max)
    }

    /// Calculate area of the AABR
    pub fn area(&self) -> f32 {
        let extent = self.extent();
        extent.x() * extent.y()
    }

    /// Check whether there is an overlap with other AABR
    pub fn overlaps(&self, other: &Aabr) -> bool {
        self.max.x() >= other.min.x()
            && self.min.x() <= other.max.x()
            && self.max.y() >= other.min.y()
            && self.min.y() <= other.max.y()
    }

    /// Check whether the AABB contains a point
    pub fn contains_point(&self, point: Vec2) -> bool {
        self.min.x() <= point.x()
            && point.x() <= self.max.x()
            && self.min.y() <= point.y()
            && point.y() <= self.max.y()
    }

    /// Move every edge by `value` along normal
    pub fn inflate(&self, value: f32) -> Aabr {
        let vector = Vec2::splat(value);
        Aabr {
            min: self.min - vector,
            max: self.max + vector,
        }
    }

    /// Calculate minkowski difference
    pub fn minkowski_difference(&self, other: &Aabr) -> Aabr {
        Aabr::new(self.min - other.max, self.max - other.min)
    }

    /// Get closest point on bounds
    pub fn closest_point_on_bounds(&self, point: Vec2) -> Vec2 {
        let mut min_dist = (point.x() - self.min.x()).abs();
        let mut bounds_pt = Vec2::new(self.min.x(), point.y());

        let dist = (self.max.x() - point.x()).abs();
        if dist < min_dist {
            min_dist = dist;
            bounds_pt = Vec2::new(self.max.x(), point.y());
        }

        let dist = (self.max.y() - point.y()).abs();
        if dist < min_dist {
            min_dist = dist;
            bounds_pt = Vec2::new(point.x(), self.max.y());
        }

        let dist = (self.min.y() - point.y()).abs();
        if dist < min_dist {
            bounds_pt = Vec2::new(point.x(), self.min.y());
        }

        bounds_pt
    }
}

impl ops::Mul<f32> for Aabr {
    type Output = Aabr;
    fn mul(self, rhs: f32) -> Aabr {
        Aabr {
            min: self.min * rhs,
            max: self.max * rhs,
        }
    }
}
