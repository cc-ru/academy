use glam::Vec2;

use crate::util::Aabr;

/// Offsets for each rectangle side (can be used to specify padding)
pub struct SideOffsets {
    /// Top offset
    pub top: f32,
    /// Right offset
    pub right: f32,
    /// Bottom offset
    pub bottom: f32,
    /// Left offset
    pub left: f32,
}

impl SideOffsets {
    /// Create a new `SideOffsets`
    pub fn new(top: f32, right: f32, bottom: f32, left: f32) -> SideOffsets {
        SideOffsets {
            top,
            right,
            bottom,
            left,
        }
    }

    /// Create a `SideOffsets` with equal offsets on each side
    pub fn new_equal(offset: f32) -> SideOffsets {
        SideOffsets::new(offset, offset, offset, offset)
    }

    /// Shrink an AABR
    pub fn shrink_aabr(&self, aabr: &Aabr) -> Aabr {
        let min = aabr.min + Vec2::new(self.top, self.left);
        let max = aabr.max - Vec2::new(self.bottom, self.right);
        Aabr { min, max }
    }

    /// Calculates `(left + right, top + bottom)` vector
    pub fn size_addition(&self) -> Vec2 {
        Vec2::new(self.left + self.right, self.top + self.bottom)
    }
}
