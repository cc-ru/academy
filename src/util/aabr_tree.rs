use super::Aabr;

use slab::Slab;

const NULL_NODE: u32 = u32::max_value();

#[derive(Clone, Debug)]
struct AabrNode<T> {
    aabr: Aabr,
    parent: u32,
    children: [u32; 2],
    data: Option<T>,
}

impl<T> AabrNode<T> {
    fn is_leaf(&self) -> bool {
        self.data.is_some()
    }
}

/// AABR tree for collision checking
#[derive(Clone, Debug)]
pub struct AabrTree<T = ()> {
    root: u32,
    nodes: Slab<AabrNode<T>>,
}

impl<T> Default for AabrTree<T> {
    fn default() -> AabrTree<T> {
        AabrTree {
            root: NULL_NODE,
            nodes: Slab::new(),
        }
    }
}

/// AABR Node ID
pub struct NodeId(u32);

impl<T> AabrTree<T> {
    /// Create an empty AABR tree
    pub fn new() -> AabrTree<T> {
        Default::default()
    }

    /// Clear the tree, without deallocating space
    pub fn clear(&mut self) {
        self.root = NULL_NODE;
        self.nodes.clear();
    }

    fn get(&self, id: u32) -> &AabrNode<T> {
        &self.nodes[id as usize]
    }

    fn get_mut(&mut self, id: u32) -> &mut AabrNode<T> {
        &mut self.nodes[id as usize]
    }

    fn alloc_node(&mut self, aabr: Aabr, data: Option<T>) -> u32 {
        let node = AabrNode {
            aabr,
            parent: NULL_NODE,
            children: [NULL_NODE; 2],
            data,
        };

        self.nodes.insert(node) as u32
    }

    /// Insert a node
    pub fn insert(&mut self, aabr: Aabr, data: T) -> NodeId {
        let node_id = self.alloc_node(aabr, Some(data));

        if self.root == NULL_NODE {
            self.root = node_id;
            return NodeId(node_id);
        }

        let mut cur_id = self.root;
        while !self.get(cur_id).is_leaf() {
            let cur = self.get(cur_id);

            let combined_aabr = aabr.union(&cur.aabr);
            let new_parent_cost = 2.0 * combined_aabr.area();
            let min_push_down_cost = 2.0 * (combined_aabr.area() - cur.aabr.area());

            let children = [self.get(cur.children[0]), self.get(cur.children[1])];
            let mut child_costs = [0.0; 2];
            for (child, child_cost) in children.iter().zip(&mut child_costs) {
                let merged_area = child.aabr.union(&aabr).area();
                *child_cost = if child.is_leaf() {
                    merged_area + min_push_down_cost
                } else {
                    merged_area - child.aabr.area() + min_push_down_cost
                }
            }

            if new_parent_cost < child_costs[0] && new_parent_cost < child_costs[1] {
                break;
            }

            cur_id = if child_costs[0] < child_costs[1] {
                cur.children[0]
            } else {
                cur.children[1]
            };
        }

        let sibling_id = cur_id;
        let sibling = self.get(sibling_id);
        let old_parent_id = sibling.parent;
        let new_parent_aabr = aabr.union(&sibling.aabr);
        let new_parent_id = self.alloc_node(new_parent_aabr, None);
        let new_parent = self.get_mut(new_parent_id);
        new_parent.children = [sibling_id, node_id];
        new_parent.parent = old_parent_id;
        self.get_mut(node_id).parent = new_parent_id;
        self.get_mut(sibling_id).parent = new_parent_id;

        if old_parent_id == NULL_NODE {
            self.root = new_parent_id;
        } else {
            let old_parent = self.get_mut(old_parent_id);
            if old_parent.children[0] == sibling_id {
                old_parent.children[0] = new_parent_id;
            } else {
                old_parent.children[1] = new_parent_id;
            }
        }

        self.fix_upwards(new_parent_id);
        NodeId(node_id)
    }

    /// Remove a node
    pub fn remove(&mut self, NodeId(id): NodeId) {
        if id == self.root {
            self.root = NULL_NODE;
            self.nodes.clear();
            return;
        }

        let parent_id = self.get(id).parent;
        let parent = self.get(parent_id);
        let sibling_id = if parent.children[0] == id {
            parent.children[1]
        } else {
            parent.children[0]
        };

        let grandparent_id = self.get(parent_id).parent;
        if grandparent_id == NULL_NODE {
            self.root = sibling_id;
            self.get_mut(sibling_id).parent = NULL_NODE;
            self.nodes.remove(parent_id as usize);
        } else {
            let grandparent = self.get_mut(grandparent_id);
            if grandparent.children[0] == parent_id {
                grandparent.children[0] = sibling_id;
            } else {
                grandparent.children[1] = sibling_id;
            }

            self.nodes.remove(parent_id as usize);
            self.get_mut(sibling_id).parent = grandparent_id;
            self.fix_upwards(grandparent_id);
        }

        self.nodes.remove(id as usize);
    }

    fn fix_upwards(&mut self, mut node_id: u32) {
        while node_id != NULL_NODE {
            let node = self.get(node_id);
            let parent = node.parent;
            let [left_id, right_id] = node.children;
            let aabr = self.get(left_id).aabr.union(&self.get(right_id).aabr);
            self.get_mut(node_id).aabr = aabr;
            node_id = parent;
        }
    }

    #[inline(always)]
    fn next_overlap(&self, aabr: &Aabr, stack: &mut Vec<u32>) -> Option<(&Aabr, &T)> {
        while let Some(node_id) = stack.pop() {
            if node_id == NULL_NODE {
                continue;
            }

            let node = self.get(node_id);
            if node.aabr.overlaps(&aabr) {
                if let Some(ref data) = node.data {
                    return Some((&node.aabr, data));
                } else {
                    stack.extend(node.children.iter().copied());
                }
            }
        }

        None
    }

    /// Check whether the given AABR overlaps with any AABR contained in this tree
    pub fn any_overlap(&self, aabr: Aabr) -> bool {
        let mut stack = Vec::new();
        stack.push(self.root);
        self.next_overlap(&aabr, &mut stack).is_some()
    }

    /// Iterate through all overlaps
    pub fn iter_overlaps(&self, aabr: Aabr) -> impl Iterator<Item = (&Aabr, &T)> + '_ {
        let mut stack = Vec::new();
        stack.push(self.root);
        std::iter::from_fn(move || self.next_overlap(&aabr, &mut stack))
    }

    /// Iterate through non leaf nodes
    pub fn iter_branches(&self) -> impl Iterator<Item = &Aabr> + '_ {
        self.nodes
            .iter()
            .filter(|(_, n)| !n.is_leaf())
            .map(|(_, n)| &n.aabr)
    }

    /// Iterate through leaf nodes
    pub fn iter_leaves(&self) -> impl Iterator<Item = (&Aabr, &T)> + '_ {
        self.nodes
            .iter()
            .flat_map(|(_, n)| n.data.as_ref().map(|d| (&n.aabr, d)))
    }
}
