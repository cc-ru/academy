use std::io::{self, Read};

use fxhash::FxHashMap;
use legion::storage::Component;
use legion::world::Duplicate;
use legion::{Entity, World};
use once_cell::sync::Lazy;

use super::Decodable;
use crate::assets::Handle;
use crate::components::{CollisionRect, Position, SpriteOrigin, SpritePath};
use crate::graphics::Sprite;

/// Component which can be decoded from bytes
pub trait DecodableComponent: Component + Decodable {
    /// Component ID
    const ID: u16;
}

type Decoder = fn(&mut dyn Read, &mut World, Entity) -> io::Result<()>;

/// Registry for decodable components
#[derive(Clone, Default)]
pub struct DecodableComponentRegistry {
    decoders: FxHashMap<u16, Decoder>,
}

impl DecodableComponentRegistry {
    /// Register a component type for decoding
    pub fn register<C: DecodableComponent>(&mut self) {
        let decoder =
            |mut source: &mut dyn Read, world: &mut World, entity: Entity| -> io::Result<()> {
                let component = C::decode(&mut source)?;
                world.entry(entity).unwrap().add_component(component);
                Ok(())
            };
        self.decoders.insert(C::ID, decoder);
    }

    /// Decode a list of components into world
    pub fn decode_component_list<R: Read>(
        &self,
        source: &mut R,
        world: &mut World,
        entity: Entity,
    ) -> io::Result<Entity> {
        let num_components = u8::decode(source)?;
        for _ in 0..num_components {
            let id = u16::decode(source)?;
            if let Some(decoder) = self.decoders.get(&id) {
                decoder(source, world, entity)?;
            }
        }

        Ok(entity)
    }
}

/// Global decodable component registry
pub static GLOBAL_REGISTRY: Lazy<DecodableComponentRegistry> = Lazy::new(|| {
    let mut registry = DecodableComponentRegistry::default();
    registry.register::<Position>();
    registry.register::<SpritePath>();
    registry.register::<CollisionRect>();
    registry.register::<SpriteOrigin>();
    registry
});

/// Get component merger to copy components
pub fn component_merger() -> Duplicate {
    let mut merger = Duplicate::new();
    merger.register_copy::<Position>();
    merger.register_clone::<Handle<Sprite>>();
    merger.register_clone::<SpritePath>();
    merger.register_copy::<CollisionRect>();
    merger.register_copy::<SpriteOrigin>();
    merger
}
