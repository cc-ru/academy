//! Map loading

mod component;
mod decode;

pub use self::component::*;
pub use self::decode::*;

use std::cmp::Ordering;
use std::fs::File;
use std::io::{self, BufReader, Cursor, Read, Seek, SeekFrom};
use std::path::{Path, PathBuf};

use fxhash::{FxHashMap, FxHashSet};
use glam::Vec2;
use legion::storage::{ArchetypeIndex, ComponentTypeId};
use legion::systems::CommandBuffer;
use legion::world::EntityStore;
use legion::{Entity, IntoQuery, World};
use once_cell::sync::Lazy;

use crate::assets::{Assets, Handle};
use crate::components::{CollisionRect, Position, SpriteOrigin, SpritePath};
use crate::graphics::Sprite;
use crate::util::AabrTree;

/// List of prefabs, represented as an ECS world
#[derive(Default)]
pub struct PrefabList {
    world: World,
    mapping: FxHashMap<u16, Entity>,
}

impl Decodable for PrefabList {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        let mut signature = [0; 3];
        source.read_exact(&mut signature)?;
        if &signature != b"PBL" {
            return Err(io::Error::new(io::ErrorKind::Other, "invalid signature"));
        }

        let length = u16::decode(source)?;

        let mut world = World::default();
        let mut mapping = FxHashMap::with_capacity_and_hasher(length as usize, Default::default());

        for id in 0..length {
            let entity = world.push(());
            GLOBAL_REGISTRY.decode_component_list(source, &mut world, entity)?;
            mapping.insert(id, entity);
        }

        Ok(PrefabList { world, mapping })
    }
}

impl PrefabList {
    /// Open prefab list at the specified path
    pub fn open(path: impl AsRef<Path>) -> io::Result<Self> {
        let file = File::open(path)?;
        let mut reader = BufReader::new(file);
        PrefabList::decode(&mut reader)
    }

    /// Resolve assets
    pub fn resolve_assets(&mut self, assets: &Assets) {
        let mut cbuf = CommandBuffer::new(&self.world);
        for (&id, sprite) in <(Entity, &SpritePath)>::query().iter(&self.world) {
            cbuf.add_component::<Handle<Sprite>>(id, assets.load(&*sprite.0));
            cbuf.remove_component::<SpritePath>(id);
        }
        cbuf.flush(&mut self.world);
    }
}

/// Chunk coordinates. A single chunk is 1024x1024 pixels or 32x32 tiles
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ChunkPosition {
    /// X
    pub x: i16,
    /// Y
    pub y: i16,
}

impl ChunkPosition {
    /// Create a new chunk position with specified coordinates
    pub fn new(x: i16, y: i16) -> ChunkPosition {
        ChunkPosition { x, y }
    }
}

impl Decodable for ChunkPosition {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(ChunkPosition {
            x: i16::decode(source)?,
            y: i16::decode(source)?,
        })
    }
}

/// Chunk metadata
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct ChunkMetadata {
    /// Position
    pub pos: ChunkPosition,
    /// Data offset from the beginning of the file
    pub offset: u32,
    /// Data size
    pub size: u32,
}

impl Decodable for ChunkMetadata {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(ChunkMetadata {
            pos: ChunkPosition::decode(source)?,
            offset: u32::decode(source)?,
            size: u32::decode(source)?,
        })
    }
}

/// Marker for floor tiles
#[derive(Clone, Copy, Debug)]
pub struct FloorTile;

/// Loaded map
pub struct Map {
    file: File,
    prefab_lists: Vec<PathBuf>,
    chunks_metadata: FxHashMap<ChunkPosition, ChunkMetadata>,
    cached_static_archetypes: FxHashSet<ArchetypeIndex>,
    /// Loaded chunks
    pub chunks: FxHashMap<ChunkPosition, Chunk>,
    /// Sorted chunks
    pub chunk_order: Vec<ChunkPosition>,
}

impl Map {
    /// Open map at the specified path
    pub fn open(path: impl AsRef<Path>) -> io::Result<Map> {
        let mut file = File::open(path)?;
        let mut reader = BufReader::new(&mut file);

        let mut signature = [0; 3];
        reader.read_exact(&mut signature)?;
        if &signature != b"MAP" {
            return Err(io::Error::new(io::ErrorKind::Other, "invalid signature"));
        }

        let length = u8::decode(&mut reader)?;
        let mut prefab_lists = Vec::with_capacity(length as usize);
        for _ in 0..length {
            prefab_lists.push(PathBuf::decode(&mut reader)?);
        }

        let num_chunks = u16::decode(&mut reader)? as usize;
        let mut chunks_metadata =
            FxHashMap::with_capacity_and_hasher(num_chunks, Default::default());
        for _ in 0..num_chunks {
            let metadata = ChunkMetadata::decode(&mut reader)?;
            chunks_metadata.insert(metadata.pos, metadata);
        }

        Ok(Map {
            file,
            prefab_lists,
            chunks_metadata,
            cached_static_archetypes: FxHashSet::default(),
            chunks: FxHashMap::default(),
            chunk_order: Vec::new(),
        })
    }

    /// Check whether the map contains chunk at the given coordinates (not necessarily loaded)
    pub fn has_chunk(&self, pos: ChunkPosition) -> bool {
        self.chunks_metadata.contains_key(&pos)
    }

    /// Load a chunk at the specified coordinates
    pub fn load_chunk(
        &mut self,
        pos: ChunkPosition,
        prefab_lists: &FxHashMap<PathBuf, PrefabList>,
        world: &mut World,
        assets: &Assets,
    ) -> io::Result<()> {
        if !self.has_chunk(pos) {
            return Err(io::Error::new(io::ErrorKind::Other, "no such chunk"));
        }

        let prefab_lists = self
            .prefab_lists
            .iter()
            .map(|path| &prefab_lists[path])
            .collect::<Vec<_>>();

        let metadata = &self.chunks_metadata[&pos];
        let mut merger = component_merger();

        self.file.seek(SeekFrom::Start(metadata.offset as u64))?;
        let mut chunk_data = vec![0; metadata.size as usize];
        self.file.read_exact(&mut chunk_data)?;

        let mut cursor = Cursor::new(chunk_data);

        let mut chunk = Chunk {
            meta: *metadata,
            tiles: ChunkTiles {
                sprites: Vec::with_capacity(64),
            },
            objects: Vec::new(),
            bvh: AabrTree::new(),
        };

        for _ in 0..64 {
            let prefab_list_id = u8::decode(&mut cursor)?;
            let prefab_id = u16::decode(&mut cursor)?;
            let prefab_list = prefab_lists[prefab_list_id as usize];
            let entity = prefab_list.mapping[&prefab_id];
            let entry = prefab_list.world.entry_ref(entity).unwrap();
            let sprite = entry
                .get_component::<Handle<Sprite>>()
                .expect("tile without sprite");
            chunk.tiles.sprites.push(sprite.clone());
        }

        let num_entities = u16::decode(&mut cursor)?;
        for _ in 0..num_entities {
            let prefab_list_id = u8::decode(&mut cursor)?;
            let prefab_id = u16::decode(&mut cursor)?;
            let prefab_list = prefab_lists[prefab_list_id as usize];
            let src_entity = prefab_list.mapping[&prefab_id];
            let entity = world.clone_from_single(&prefab_list.world, src_entity, &mut merger);
            GLOBAL_REGISTRY.decode_component_list(&mut cursor, world, entity)?;

            let mut entry = world.entry(entity).unwrap();

            if let Ok(path) = entry.get_component::<SpritePath>() {
                let path = (*path.0).clone();
                entry.add_component::<Handle<Sprite>>(assets.load(path));
                entry.remove_component::<SpritePath>();
            }

            let archetype = entry.archetype();
            let layout = archetype.layout();

            let mut is_static = true;
            let cached = self.cached_static_archetypes.contains(&archetype.index());

            if !cached && !is_static {
                for &comp_id in REQUIRED_STATIC_COMPONENTS.iter() {
                    if !layout.has_component_by_id(comp_id) {
                        is_static = false;
                        break;
                    }
                }
            }

            if !cached && !is_static {
                for comp_id in layout.component_types() {
                    if STATIC_COMPONENTS.contains(comp_id) {
                        is_static = false;
                        break;
                    }
                }
            }

            if is_static {
                let sprite = entry.get_component::<Handle<Sprite>>().unwrap().clone();

                let pos = entry
                    .get_component::<Position>()
                    .map(|v| v.0)
                    .unwrap_or_else(|_| Vec2::zero());
                let origin = entry
                    .get_component::<SpriteOrigin>()
                    .map(|v| v.0)
                    .unwrap_or_else(|_| Vec2::zero());
                chunk.objects.push(ChunkObject {
                    sprite,
                    pos,
                    origin,
                });

                if let Ok(&CollisionRect(aabr)) = entry.get_component() {
                    chunk.bvh.insert(aabr.translate(pos), ());
                }

                world.remove(entity);
            } else {
                entry.add_component(metadata.pos); // add chunk pos to unload entity later
            }
        }

        chunk.objects.sort_unstable_by(|a, b| {
            let a = a.pos;
            let b = b.pos;
            a.y()
                .partial_cmp(&b.y())
                .unwrap_or(Ordering::Greater)
                .then_with(|| a.x().partial_cmp(&b.x()).unwrap_or(Ordering::Greater))
        });

        self.chunks.insert(metadata.pos, chunk);
        self.chunk_order.push(metadata.pos);

        self.chunk_order
            .sort_unstable_by(|a, b| a.y.cmp(&b.y).then_with(|| a.x.cmp(&b.x)));

        Ok(())
    }

    /// Check wheter chunk at the specified coordinates is loaded
    pub fn is_chunk_loaded(&self, pos: ChunkPosition) -> bool {
        self.chunks.contains_key(&pos)
    }

    /// Unload chunk at the specified coordinates
    pub fn unload_chunk(&mut self, pos: ChunkPosition, world: &mut World) {
        self.chunks.remove(&pos);

        let mut cbuf = CommandBuffer::new(&world);

        for (&entity, &chunk) in <(&Entity, &ChunkPosition)>::query().iter(world) {
            if chunk == pos {
                cbuf.remove(entity);
            }
        }

        cbuf.flush(world);
        self.chunk_order
            .remove(self.chunk_order.iter().position(|&v| v == pos).unwrap());
    }
}

/// Loaded chunk
#[derive(Clone, Debug)]
pub struct Chunk {
    /// Metadata
    pub meta: ChunkMetadata,
    /// Background tiles
    pub tiles: ChunkTiles,
    /// Foreground objects (which are not a part of ECS). Sorted by origin (Y then X)
    pub objects: Vec<ChunkObject>,
    /// AABR tree for collisions
    pub bvh: AabrTree,
}

/// Chunk tiles
#[derive(Clone, Debug)]
pub struct ChunkTiles {
    /// Tile sprites
    pub sprites: Vec<Handle<Sprite>>,
}

/// Chunk object
#[derive(Clone, Debug)]
pub struct ChunkObject {
    /// Sprite
    pub sprite: Handle<Sprite>,
    /// Position
    pub pos: Vec2,
    /// Origin
    pub origin: Vec2,
}

/// Components which can be added to a chunk object
static STATIC_COMPONENTS: Lazy<FxHashSet<ComponentTypeId>> = Lazy::new(|| {
    let mut set = FxHashSet::default();
    set.insert(ComponentTypeId::of::<Handle<Sprite>>());
    set.insert(ComponentTypeId::of::<SpriteOrigin>());
    set.insert(ComponentTypeId::of::<Position>());
    set.insert(ComponentTypeId::of::<CollisionRect>());
    set
});

/// Components which are required for a chunk object
static REQUIRED_STATIC_COMPONENTS: Lazy<FxHashSet<ComponentTypeId>> = Lazy::new(|| {
    let mut set = FxHashSet::default();
    set.insert(ComponentTypeId::of::<Handle<Sprite>>());
    set.insert(ComponentTypeId::of::<Position>());
    set
});
