use std::io::{self, Read};
use std::path::PathBuf;

use byteorder::{LittleEndian, ReadBytesExt};
use glam::Vec2;

use crate::util::Aabr;

/// Trait from decoding binary data
pub trait Decodable: Sized {
    /// Read needed amount of bytes from the source and try to decode them
    fn decode<R: Read>(source: &mut R) -> io::Result<Self>;
}

impl Decodable for u8 {
    fn decode<R: Read>(source: &mut R) -> io::Result<u8> {
        source.read_u8()
    }
}

impl Decodable for u16 {
    fn decode<R: Read>(source: &mut R) -> io::Result<u16> {
        source.read_u16::<LittleEndian>()
    }
}

impl Decodable for i16 {
    fn decode<R: Read>(source: &mut R) -> io::Result<i16> {
        source.read_i16::<LittleEndian>()
    }
}

impl Decodable for u32 {
    fn decode<R: Read>(source: &mut R) -> io::Result<u32> {
        source.read_u32::<LittleEndian>()
    }
}

impl Decodable for f32 {
    fn decode<R: Read>(source: &mut R) -> io::Result<f32> {
        source.read_f32::<LittleEndian>()
    }
}

impl Decodable for PathBuf {
    fn decode<R: Read>(source: &mut R) -> io::Result<PathBuf> {
        let length = u16::decode(source)?;
        let mut buf = vec![0; length as usize];
        source.read_exact(&mut buf)?;
        Ok(String::from_utf8(buf)
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "invalid utf8"))?
            .into())
    }
}

impl Decodable for Vec2 {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(Vec2::new(f32::decode(source)?, f32::decode(source)?))
    }
}

impl Decodable for Aabr {
    fn decode<R: Read>(source: &mut R) -> io::Result<Self> {
        Ok(Aabr::new(Vec2::decode(source)?, Vec2::decode(source)?))
    }
}
